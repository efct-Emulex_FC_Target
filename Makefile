#/*******************************************************************
# * This file is part of the Emulex Linux Device Driver for         *
# * Fibre Channel Host Bus Adapters.                                *
# * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
# * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
# *                                                                 *
# * This program is free software; you can redistribute it and/or   *
# * modify it under the terms of version 2 of the GNU General       *
# * Public License as published by the Free Software Foundation.    *
# * This program is distributed in the hope that it will be useful. *
# * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
# * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
# * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
# * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
# * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
# * more details, a copy of which can be found in the file COPYING  *
# * included with this package.                                     *
# ********************************************************************/

obj-$(CONFIG_SCSI_EFCT) := efct.o

efct-objs := efct/efct_driver.o efct/efct_io.o efct/efct_scsi.o efct/efct_els.o \
	     efct/efct_xport.o efct/efct_hw.o efct/efct_hw_queues.o \
	     efct/efct_utils.o efct/efct_lio.o efct/efct_unsol.o

efct-objs += libefc/efc_domain.o libefc/efc_fabric.o libefc/efc_node.o \
	     libefc/efc_sport.o libefc/efc_device.o \
	     libefc/efc_lib.o libefc/efc_sm.o

efct-objs += libefc_sli/sli4.o
