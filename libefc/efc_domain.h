/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

/*
 * Declare driver's domain handler exported interface
 */

#if !defined(__EFCT_DOMAIN_H__)
#define __EFCT_DOMAIN_H__

#define SLI4_MAX_FCFI 64
extern int
efc_domain_init(struct efc_lport *efc, struct efc_domain_s *domain);
extern struct efc_domain_s *
efc_domain_find(struct efc_lport *efc, uint64_t fcf_wwn);
extern struct efc_domain_s *
efc_domain_alloc(struct efc_lport *efc, uint64_t fcf_wwn);
extern void
efc_domain_free(struct efc_domain_s *domain);

static inline void
efc_domain_lock_init(struct efc_domain_s *domain)
{
}

static inline bool
efc_domain_lock_try(struct efc_domain_s *domain)
{
	/* Use the device wide lock */
	return efc_device_lock_try(domain->efc);
}

static inline void
efc_domain_lock(struct efc_domain_s *domain)
{
	/* Use the device wide lock */
	efc_device_lock(domain->efc);
}

static inline void
efc_domain_unlock(struct efc_domain_s *domain)
{
	/* Use the device wide lock */
	efc_device_unlock(domain->efc);
}

extern void *
__efc_domain_init(struct efc_sm_ctx_s *ctx,
		  enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_wait_alloc(struct efc_sm_ctx_s *ctx,
			enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_allocated(struct efc_sm_ctx_s *ctx,
		       enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_wait_attach(struct efc_sm_ctx_s *ctx,
			 enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_ready(struct efc_sm_ctx_s *ctx,
		   enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_wait_sports_free(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_wait_shutdown(struct efc_sm_ctx_s *ctx,
			   enum efc_sm_event_e evt, void *arg);
extern void *
__efc_domain_wait_domain_lost(struct efc_sm_ctx_s *ctx,
			      enum efc_sm_event_e evt, void *arg);

extern void
efc_domain_attach(struct efc_domain_s *domain, u32 s_id);
extern int
efc_domain_post_event(struct efc_domain_s *domain,
		      enum efc_sm_event_e event, void *arg);
extern void
__efc_domain_attach_internal(struct efc_domain_s *domain, u32 s_id);

extern struct efc_domain_s *
efc_domain_get(struct efc_lport *efc, uint16_t fcfi);

#endif /* __EFCT_DOMAIN_H__ */
