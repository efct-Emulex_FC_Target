/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include <linux/module.h>
#include <linux/kernel.h>
#include "efc.h"

unsigned int efc_log_level = EFC_LOG_LIB;

int efcport_init(struct efc_lport *efc)
{
	u32 rc = 0;
	bool node_pool_created = false;

	efc_device_lock_init(efc);

	INIT_LIST_HEAD(&efc->domain_list);
	INIT_LIST_HEAD(&efc->vport_list);

	/* Create Node pool */
	rc = efc_node_create_pool(efc, EFC_MAX_REMOTE_NODES);
	if (rc) {
		efc_log_err(efc, "Can't allocate node pool\n");
		goto cleanup;
	} else {
		node_pool_created = true;
	}

	return rc;
cleanup:
	if (node_pool_created)
		efc_node_free_pool(efc);

	return -1;
}

void efcport_destroy(struct efc_lport *efc)
{
	efc_node_free_pool(efc);
}

/**
 * @brief Sparse Vector API.
 *
 * This is a trimmed down sparse vector implementation tuned to the problem of
 * 24-bit FC_IDs. In this case, the 24-bit index value is broken down in three
 * 8-bit values. These values are used to index up to three 256 element arrays.
 * Arrays are allocated, only when needed. @n @n
 * The lookup can complete in constant time (3 indexed array references). @n @n
 * A typical use case would be that the fabric/directory FC_IDs would cause two
 * rows to be allocated, and the fabric assigned remote nodes would cause two
 * rows to be allocated, with the root row always allocated. This gives five
 * rows of 256 x sizeof(void*), resulting in 10k.
 */

/**
 * @ingroup spv
 * @brief Allocate a new sparse vector row.
 *
 * @param os OS handle
 * @param rowcount Count of rows.
 *
 * @par Description
 * A new sparse vector row is allocated.
 *
 * @param rowcount Number of elements in a row.
 *
 * @return Returns the pointer to a row.
 */
static void
**efc_spv_new_row(u32 rowcount)
{
	return kzalloc(sizeof(void *) * rowcount, GFP_ATOMIC);
}

/**
 * @ingroup spv
 * @brief Delete row recursively.
 *
 * @par Description
 * This function recursively deletes the rows in this sparse vector
 *
 * @param os OS handle
 * @param a Pointer to the row.
 * @param n Number of elements in the row.
 * @param depth Depth of deleting.
 *
 * @return None.
 */
static void
_efc_spv_del(void *os, void **a, u32 n, u32 depth)
{
	if (a) {
		if (depth) {
			u32 i;

			for (i = 0; i < n; i++)
				_efc_spv_del(os, a[i], n, depth - 1);

			kfree(a);
		}
	}
}

/**
 * @ingroup spv
 * @brief Delete a sparse vector.
 *
 * @par Description
 * The sparse vector is freed.
 *
 * @param spv Pointer to the sparse vector object.
 */
void
efc_spv_del(struct sparse_vector_s *spv)
{
	if (spv) {
		_efc_spv_del(spv->os, spv->array, SPV_ROWLEN, SPV_DIM);
		kfree(spv);
	}
}

/**
 * @ingroup spv
 * @brief Instantiate a new sparse vector object.
 *
 * @par Description
 * A new sparse vector is allocated.
 *
 * @param os OS handle
 *
 * @return Returns the pointer to the sparse vector, or NULL.
 */
struct sparse_vector_s
*efc_spv_new(void *os)
{
	struct sparse_vector_s *spv;
	u32 i;

	spv = kzalloc(sizeof(*spv), GFP_ATOMIC);
	if (!spv)
		return NULL;

	spv->os = os;
	spv->max_idx = 1;
	for (i = 0; i < SPV_DIM; i++)
		spv->max_idx *= SPV_ROWLEN;

	return spv;
}

/**
 * @ingroup spv
 * @brief Return the address of a cell.
 *
 * @par Description
 * Returns the address of a cell, allocates sparse rows as needed if the
 *         alloc_new_rows parameter is set.
 *
 * @param sv Pointer to the sparse vector.
 * @param idx Index of which to return the address.
 * @param alloc_new_rows If TRUE, then new rows may be allocated to set values,
 *                       Set to FALSE for retrieving values.
 *
 * @return Returns the pointer to the cell, or NULL.
 */
static void
*efc_spv_new_cell(struct sparse_vector_s *sv, u32 idx,
		   bool alloc_new_rows)
{
	u32 a = (idx >> 16) & 0xff;
	u32 b = (idx >>  8) & 0xff;
	u32 c = (idx >>  0) & 0xff;
	void **p;

	if (idx >= sv->max_idx)
		return NULL;

	if (!sv->array) {
		sv->array = (alloc_new_rows ?
			     efc_spv_new_row(SPV_ROWLEN) : NULL);
		if (!sv->array)
			return NULL;
	}
	p = sv->array;
	if (!p[a]) {
		p[a] = (alloc_new_rows ? efc_spv_new_row(SPV_ROWLEN) : NULL);
		if (!p[a])
			return NULL;
	}
	p = p[a];
	if (!p[b]) {
		p[b] = (alloc_new_rows ? efc_spv_new_row(SPV_ROWLEN) : NULL);
		if (!p[b])
			return NULL;
	}
	p = p[b];

	return &p[c];
}

/**
 * @ingroup spv
 * @brief Set the sparse vector cell value.
 *
 * @par Description
 * Sets the sparse vector at @c idx to @c value.
 *
 * @param sv Pointer to the sparse vector.
 * @param idx Index of which to store.
 * @param value Value to store.
 *
 * @return None.
 */
void
efc_spv_set(struct sparse_vector_s *sv, u32 idx, void *value)
{
	void **ref = efc_spv_new_cell(sv, idx, true);

	if (ref)
		*ref = value;
}

/**
 * @ingroup spv
 * @brief Return the sparse vector cell value.
 *
 * @par Description
 * Returns the value at @c idx.
 *
 * @param sv Pointer to the sparse vector.
 * @param idx Index of which to return the value.
 *
 * @return Returns the cell value, or NULL.
 */
void
*efc_spv_get(struct sparse_vector_s *sv, u32 idx)
{
	void **ref = efc_spv_new_cell(sv, idx, false);

	if (ref)
		return *ref;

	return NULL;
}

/*
 * @brief copy into dma buffer
 *
 * Copies into a dma buffer, updates the len element
 *
 * @param dma DMA descriptor
 * @param buffer address of buffer to copy from
 * @param buffer_length buffer length in bytes
 *
 * @return returns bytes copied for success,
 * a negative error code value for failure.
 */

int
efc_dma_copy_in(struct efc_dma_s *dma, void *buffer, u32 buffer_length)
{
	if (!dma)
		return -1;
	if (!buffer)
		return -1;
	if (buffer_length == 0)
		return 0;
	if (buffer_length > dma->size)
		buffer_length = dma->size;
	memcpy(dma->virt, buffer, buffer_length);
	dma->len = buffer_length;
	return buffer_length;
}

/**
 * @brief Initialize recursive lock
 *
 * Initialize a recursive lock
 *
 * @param efc pointer to efc structure
 * @param lock pointer to recursive lock
 * @param name text
 *
 * @return none
 */

void
efc_rlock_init(struct efc_lport *efc, struct efc_rlock_s *lock,
	       const char *name)
{
	memset(lock, 0, sizeof(*lock));

	spin_lock_init(&lock->lock);

	lock->pid.l = ~0;

	lock->efc = efc;
	lock->name = name;
}

/**
 * @brief try to acquire a recursive lock given pid
 *
 * Attempt to acquire a recursive lock, return TRUE if successful
 *
 * @param lock pointer to recursive lock
 * @param pid thread id of caller
 *
 * @return TRUE if lock was acquired, FALSE if not
 */

static int
efc_rlock_try_pid(struct efc_rlock_s *lock, union efc_pid_u pid)
{
	unsigned long flags = 0;
	bool rc = false;

	spin_lock_irqsave(&lock->lock, flags);
	if (lock->count > 0) {
		if (pid.l == lock->pid.l) {
			lock->count++;
			rc = true;
		}
	} else {
		lock->pid = pid;
		lock->count++;
		rc = true;
	}
	spin_unlock_irqrestore(&lock->lock, flags);
	return rc;
}

/**
 * @brief try to acquire a recursive lock
 *
 * Attempt to acquire a recursive lock, return TRUE if successful
 *
 * @param lock pointer to recursive lock
 *
 * @return TRUE if lock was acquired, FALSE if not
 */

bool efc_rlock_try(struct efc_rlock_s *lock)
{
	union efc_pid_u pid = efc_mkpid();

	return efc_rlock_try_pid(lock, pid);
}

/**
 * @brief acquire recursive lock
 *
 * Acquire recursive lock, return when successful
 *
 * @param lock pointer to recursive lock
 *
 * @return none
 */

void
efc_rlock_acquire(struct efc_rlock_s *lock)
{
	union efc_pid_u pid = efc_mkpid();

	while (efc_rlock_try_pid(lock, pid) == false)
		;
}

/**
 * @brief release recursive lock
 *
 * Release a previously acquired recursive lock
 *
 * @param lock pointer to recursive lock
 *
 * @return none
 */

void
efc_rlock_release(struct efc_rlock_s *lock)
{
	union efc_pid_u pid = efc_mkpid();
	unsigned long flags = 0;

	spin_lock_irqsave(&lock->lock, flags);

	if (pid.l != lock->pid.l) {
		efc_log_test(lock->efc, "pid mismatch\n");
		spin_unlock_irqrestore(&lock->lock, flags);
		return;
	}
	if (lock->count == 0) {
		efc_log_test(lock->efc, "count is zero\n");
		spin_unlock_irqrestore(&lock->lock, flags);
		return;
	}

	lock->count--;
	if (lock->count == 0) {
		/* Set the PID to some invalid but recognizable value */
		lock->pid.l = ~0;
	}

	spin_unlock_irqrestore(&lock->lock, flags);
}

int
efc_sem_init(struct efc_sem_s *sem, int val, const char *name, ...)
{
	va_list ap;

	va_start(ap, name);
	vsnprintf((char *)sem->name, sizeof(sem->name), name, ap);
	va_end(ap);

	sema_init(&sem->sem, val);
	return 0;
}
