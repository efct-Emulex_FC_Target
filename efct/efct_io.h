/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCT_IO_H__)
#define __EFCT_IO_H__

#define io_error_log(io, fmt, ...)  \
	do { \
		if (EFCT_LOG_ENABLE_IO_ERRORS(io->efct)) \
			efct_log_warn(io->efct, fmt, ##__VA_ARGS__); \
	} while (0)

/**
 * @brief FCP IO context
 *
 * This structure is used for transport and backend IO requests and responses.
 */

#define SCSI_CMD_BUF_LENGTH		48
#define SCSI_RSP_BUF_LENGTH		sizeof(struct fcp_rsp_iu_s)
#define EFCT_NUM_SCSI_IOS               8192

#include "efct_lio.h"
/**
 * @brief EFCT IO types
 */
enum efct_io_type_e {
	EFCT_IO_TYPE_IO = 0,
	EFCT_IO_TYPE_ELS,
	EFCT_IO_TYPE_CT,
	EFCT_IO_TYPE_CT_RESP,
	EFCT_IO_TYPE_BLS_RESP,
	EFCT_IO_TYPE_ABORT,

	EFCT_IO_TYPE_MAX,		/* must be last */
};

enum efct_els_state_e {
	EFCT_ELS_REQUEST = 0,
	EFCT_ELS_REQUEST_DELAYED,
	EFCT_ELS_REQUEST_DELAY_ABORT,
	EFCT_ELS_REQ_ABORT,
	EFCT_ELS_REQ_ABORTED,
	EFCT_ELS_ABORT_IO_COMPL,
};

struct efct_io_s {
	struct list_head list_entry;
	struct list_head io_pending_link;
	/* reference counter and callback function */
	struct kref ref;
	void (*release)(struct kref *arg);
	/* pointer back to efct */
	struct efct_s *efct;
	/* unique instance index value */
	u32 instance_index;
	/* display name */
	const char *display_name;
	/* pointer to node */
	struct efc_node_s *node;
	/* (io_pool->io_free_list) free list link */
	/* initiator task tag (OX_ID) for back-end and SCSI logging */
	u32 init_task_tag;
	/* target task tag (RX_ID) - for back-end and SCSI logging */
	u32 tgt_task_tag;
	/* HW layer unique IO id - for back-end and SCSI logging */
	u32 hw_tag;
	/* unique IO identifier */
	u32 tag;
	/* SGL */
	struct efct_scsi_sgl_s *sgl;
	/* Number of allocated SGEs */
	u32 sgl_allocated;
	/* Number of SGEs in this SGL */
	u32 sgl_count;
	/* backend target private IO data */
	struct efct_scsi_tgt_io_s tgt_io;
	/* expected data transfer length, based on FC header */
	u32 exp_xfer_len;

	/* Declarations private to HW/SLI */
	void *hw_priv;			/* HW private context */

	/* Declarations private to FC Transport */

	/* indicates what this struct efct_io_s structure is used for */
	enum efct_io_type_e io_type;
	/* pointer back to dslab allocation object */
	void *dslab_item;
	struct efct_hw_io_s *hio;		/* HW IO context */
	size_t transferred;		/* Number of bytes transferred so far */

	/* set if auto_trsp was set */
	bool auto_resp;
	/* set if low latency request */
	bool low_latency;
	/* selected WQ steering request */
	u8 wq_steering;
	/* selected WQ class if steering is class */
	u8 wq_class;
	/* transfer size for current request */
	u64 xfer_req;
	/* initiator callback function */
	efct_scsi_rsp_io_cb_t scsi_ini_cb;
	/* initiator callback function argument */
	void *scsi_ini_cb_arg;
	/* target callback function */
	efct_scsi_io_cb_t scsi_tgt_cb;
	/* target callback function argument */
	void *scsi_tgt_cb_arg;
	/* abort callback function */
	efct_scsi_io_cb_t abort_cb;
	/* abort callback function argument */
	void *abort_cb_arg;
	/* BLS callback function */
	efct_scsi_io_cb_t bls_cb;
	/* BLS callback function argument */
	void *bls_cb_arg;
	/* TMF command being processed */
	enum efct_scsi_tmf_cmd_e tmf_cmd;
	/* rx_id from the ABTS that initiated the command abort */
	u16 abort_rx_id;

	/* True if this is a Target command */
	bool cmd_tgt;
	/* when aborting, indicates ABTS is to be sent */
	bool send_abts;
	/* True if this is an Initiator command */
	bool cmd_ini;
	/* True if local node has sequence initiative */
	bool seq_init;
	/* iparams for hw io send call */
	union efct_hw_io_param_u iparam;
	/* HW formatted DIF parameters */
	struct efct_hw_dif_info_s hw_dif;
	/* DIF info saved for DIF error recovery */
	struct efct_scsi_dif_info_s scsi_dif_info;
	/* HW IO type */
	enum efct_hw_io_type_e hio_type;
	/* wire length */
	u64 wire_len;
	/* saved HW callback */
	void *hw_cb;
	/* Overflow SGL */
	struct efc_dma_s ovfl_sgl;

	/* for ELS requests/responses */
	/* True if ELS is pending */
	bool els_pend;
	/* True if ELS is active */
	bool els_active;
	/* ELS request payload buffer */
	struct efc_dma_s els_req;
	/* ELS response payload buffer */
	struct efc_dma_s els_rsp;
	/* EIO IO state machine context */
	//struct efc_sm_ctx_s els_sm;
	/* current event posting nesting depth */
	//uint els_evtdepth;
	/* this els is to be free'd */
	bool els_req_free;
	/* Retries remaining */
	u32 els_retries_remaining;
	void (*els_callback)(struct efc_node_s *node,
			     struct efc_node_cb_s *cbdata, void *cbarg);
	void *els_callback_arg;
	/* timeout */
	u32 els_timeout_sec;

	/* delay timer */
	struct timer_list delay_timer;

	/* for abort handling */
	/* pointer to IO to abort */
	struct efct_io_s *io_to_abort;

	enum efct_els_state_e	state;
	/* Protects els cmds */
	spinlock_t	els_lock;

	/* SCSI Command buffer, used for CDB (initiator) */
	struct efc_dma_s cmdbuf;
	/* SCSI Response buffer (i+t) */
	struct efc_dma_s rspbuf;
	/* Timeout value in seconds for this IO */
	u32  timeout;
	/* CS_CTL priority for this IO */
	u8   cs_ctl;
	/* Is io object in freelist > */
	u8	  io_free;
	u32  app_id;
};

/**
 * @brief common IO callback argument
 *
 * Callback argument used as common I/O callback argument
 */

struct efct_io_cb_arg_s {
	int status;		/* completion status */
	int ext_status;	/* extended completion status */
	void *app;		/* application argument */
};

/**
 * @brief Test if IO object is busy
 *
 * Return True if IO object is busy.
 * Busy is defined as the IO object not being on the free list
 *
 * @param io Pointer to IO object
 *
 * @return returns True if IO is busy
 */

static inline int
efct_io_busy(struct efct_io_s *io)
{
	return !(io->io_free);
}

struct efct_io_pool_s *
efct_io_pool_create(struct efct_s *efct, u32 num_io, u32 num_sgl);
extern int
efct_io_pool_free(struct efct_io_pool_s *io_pool);
extern u32
efct_io_pool_allocated(struct efct_io_pool_s *io_pool);

extern struct efct_io_s *
efct_io_pool_io_alloc(struct efct_io_pool_s *io_pool);
extern void
efct_io_pool_io_free(struct efct_io_pool_s *io_pool, struct efct_io_s *io);
extern struct efct_io_s *
efct_io_find_tgt_io(struct efct_s *efct, struct efc_node_s *node,
		    u16 ox_id, u16 rx_id);
#endif /* __EFCT_IO_H__ */

