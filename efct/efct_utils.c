/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#include "efct_driver.h"
#include "efct_utils.h"

#define DEFAULT_SLAB_LEN		(64 * 1024)

struct pool_hdr_s {
	struct list_head list_entry;
};

struct efct_array_s {
	void *os;

	u32 size;
	u32 count;

	u32 n_rows;
	u32 elems_per_row;
	u32 bytes_per_row;

	void **array_rows;
	u32 array_rows_len;
};

static u32 slab_len = DEFAULT_SLAB_LEN;

/**
 * @brief Void pointer array structure
 *
 * This structure describes an object consisting of an array of void
 * pointers.   The object is allocated with a maximum array size, entries
 * are then added to the array with while maintaining an entry count.   A set of
 * iterator APIs are included to allow facilitate cycling through the array
 * entries in a circular fashion.
 *
 */
struct efct_varray_s {
	void *os;
	u32 array_count;	/*>> maximum entry count in array */
	void **array;		/*>> pointer to allocated array memory */
	u32 entry_count;	/*>> number of entries added to the array */
	uint next_index;	/*>> iterator next index */
	spinlock_t lock;	/*>> iterator lock */
};

/**
 * @brief Set array slab allocation length
 *
 * The slab length is the maximum allocation length that the array uses.
 * The default 64k slab length may be overridden using this function.
 *
 * @param len new slab length.
 *
 * @return none
 */
void
efct_array_set_slablen(u32 len)
{
	slab_len = len;
}

/**
 * @brief Allocate an array object
 *
 * An array object of size and number of elements is allocated
 *
 * @param os OS handle
 * @param size size of array elements in bytes
 * @param count number of elements in array
 *
 * @return pointer to array object or NULL
 */
struct efct_array_s *
efct_array_alloc(void *os, u32 size, u32 count)
{
	struct efct_array_s *array = NULL;
	u32 i;

	/* Fail if the item size exceeds slab_len - caller should increase
	 * slab_size, or not use this API.
	 */
	if (size > slab_len) {
		efct_log_err(NULL, "Error: size exceeds slab length\n");
		return NULL;
	}

	array = kmalloc(sizeof(*array), GFP_ATOMIC);
	if (!array)
		return NULL;

	memset(array, 0, sizeof(*array));
	array->os = os;
	array->size = size;
	array->count = count;
	array->elems_per_row = slab_len / size;
	array->n_rows = (count + array->elems_per_row - 1) /
			array->elems_per_row;
	array->bytes_per_row = array->elems_per_row * array->size;

	array->array_rows_len = array->n_rows * sizeof(*array->array_rows);
	array->array_rows = kmalloc(array->array_rows_len, GFP_ATOMIC);
	if (!array->array_rows) {
		efct_array_free(array);
		return NULL;
	}
	memset(array->array_rows, 0, array->array_rows_len);
	for (i = 0; i < array->n_rows; i++) {
		array->array_rows[i] = kmalloc(array->bytes_per_row,
					       GFP_ATOMIC);
		if (!array->array_rows[i]) {
			efct_array_free(array);
			return NULL;
		}
		memset(array->array_rows[i], 0, array->bytes_per_row);
	}

	return array;
}

/**
 * @brief Free an array object
 *
 * Frees a prevously allocated array object
 *
 * @param array pointer to array object
 *
 * @return none
 */
void
efct_array_free(struct efct_array_s *array)
{
	u32 i;

	if (array) {
		if (array->array_rows) {
			for (i = 0; i < array->n_rows; i++)
				kfree(array->array_rows[i]);

			kfree(array->array_rows);
		}
		kfree(array);
	}
}

/**
 * @brief Return reference to an element of an array object
 *
 * Return the address of an array element given an index
 *
 * @param array pointer to array object
 * @param idx array element index
 *
 * @return rointer to array element, or NULL if index out of range
 */
void *efct_array_get(struct efct_array_s *array, u32 idx)
{
	void *entry = NULL;

	if (idx < array->count) {
		u32 row = idx / array->elems_per_row;
		u32 offset = idx % array->elems_per_row;

		entry = ((u8 *)array->array_rows[row]) +
			 (offset * array->size);
	}
	return entry;
}

/**
 * @brief Return number of elements in an array
 *
 * Return the number of elements in an array
 *
 * @param array pointer to array object
 *
 * @return returns count of elements in an array
 */
u32
efct_array_get_count(struct efct_array_s *array)
{
	return array->count;
}

/**
 * @brief Return size of array elements in bytes
 *
 * Returns the size in bytes of each array element
 *
 * @param array pointer to array object
 *
 * @return size of array element
 */
u32
efct_array_get_size(struct efct_array_s *array)
{
	return array->size;
}

/**
 * @brief Allocate a void pointer array
 *
 * A void pointer array of given length is allocated.
 *
 * @param os OS handle
 * @param array_count Array size
 *
 * @return returns a pointer to the efct_varray_s object, other NULL on error
 */
struct efct_varray_s *
efct_varray_alloc(void *os, u32 array_count)
{
	struct efct_varray_s *va;

	va = kmalloc(sizeof(*va), GFP_ATOMIC);
	if (va) {
		memset(va, 0, sizeof(*va));
		va->os = os;
		va->array_count = array_count;
		va->array = kmalloc(sizeof(*va->array) * va->array_count,
				    GFP_ATOMIC);
		if (va->array) {
			va->next_index = 0;
			spin_lock_init(&va->lock);
		} else {
			kfree(va);
			va = NULL;
		}
	}
	return va;
}

/**
 * @brief Free a void pointer array
 *
 * The void pointer array object is free'd
 *
 * @param va Pointer to void pointer array
 *
 * @return none
 */
void
efct_varray_free(struct efct_varray_s *va)
{
	if (va) {
		kfree(va->array);
		kfree(va);
	}
}

/**
 * @brief Add an entry to a void pointer array
 *
 * An entry is added to the void pointer array
 *
 * @param va Pointer to void pointer array
 * @param entry Pointer to entry to add
 *
 * @return returns 0 if entry was added, -1 if there is no more space in the
 * array
 */
int
efct_varray_add(struct efct_varray_s *va, void *entry)
{
	u32 rc = -1;
	unsigned long flags = 0;

	spin_lock_irqsave(&va->lock, flags);
		if (va->entry_count < va->array_count) {
			va->array[va->entry_count++] = entry;
			rc = 0;
		}
	spin_unlock_irqrestore(&va->lock, flags);

	return rc;
}

/**
 * @brief Reset the void pointer array iterator
 *
 * The next index value of the void pointer array iterator is cleared.
 *
 * @param va Pointer to void pointer array
 *
 * @return none
 */
void
efct_varray_iter_reset(struct efct_varray_s *va)
{
	unsigned long flags = 0;

	spin_lock_irqsave(&va->lock, flags);
		va->next_index = 0;
	spin_unlock_irqrestore(&va->lock, flags);
}

/**
 * @brief Return next entry from a void pointer array
 *
 * The next entry in the void pointer array is returned.
 *
 * @param va Pointer to void point array
 *
 * Note: takes the void pointer array lock
 *
 * @return returns next void pointer entry
 */
void *
efct_varray_iter_next(struct efct_varray_s *va)
{
	void *rval = NULL;
	unsigned long flags = 0;

	if (va) {
		spin_lock_irqsave(&va->lock, flags);
			rval = _efct_varray_iter_next(va);
		spin_unlock_irqrestore(&va->lock, flags);
	}
	return rval;
}

/**
 * @brief Return next entry from a void pointer array
 *
 * The next entry in the void pointer array is returned.
 *
 * @param va Pointer to void point array
 *
 * Note: doesn't take the void pointer array lock
 *
 * @return returns next void pointer entry
 */
void *
_efct_varray_iter_next(struct efct_varray_s *va)
{
	void *rval;

	rval = va->array[va->next_index];
	if (++va->next_index >= va->entry_count)
		va->next_index = 0;
	return rval;
}

/**
 * @brief Return entry count for a void pointer array
 *
 * The entry count for a void pointer array is returned
 *
 * @param va Pointer to void pointer array
 *
 * @return returns entry count
 */
u32
efct_varray_get_count(struct efct_varray_s *va)
{
	u32 rc;
	unsigned long flags = 0;

	spin_lock_irqsave(&va->lock, flags);
		rc = va->entry_count;
	spin_unlock_irqrestore(&va->lock, flags);
	return rc;
}

/**
 * The efct_pool_s data structure consists of:
 *
 *	pool->a		An efct_array_s.
 *	pool->freelist	A linked list of free items.
 *
 *	When a pool is allocated using efct_pool_alloc(), the caller
 *	provides the size in bytes of each memory pool item (size), and
 *	a count of items (count). Since efct_pool_alloc() has no visibility
 *	into the object the caller is allocating, a link for the linked list
 *	is "pre-pended".  Thus when allocating the efct_array_s, the size used
 *	is the size of the pool_hdr_s plus the requestedmemory pool item size.
 *
 *	array item layout:
 *
 *		pool_hdr_s
 *		pool data[size]
 *
 *	The address of the pool data is returned when allocated (using
 *	efct_pool_get(), or efct_pool_get_instance()), and received when being
 *	freed (using efct_pool_put(). So the address returned by the array item
 *	(efct_array_get()) must be offset by the size of pool_hdr_s.
 */

/**
 * @brief Allocate a memory pool.
 *
 * A memory pool of given size and item count is allocated.
 *
 * @param os OS handle.
 * @param size Size in bytes of item.
 * @param count Number of items in a memory pool.
 * @param use_lock TRUE to enable locking of pool.
 *
 * @return Returns pointer to allocated memory pool, or NULL.
 */
struct efct_pool_s *
efct_pool_alloc(void *os, u32 size, u32 count,
		bool use_lock)
{
	struct efct_pool_s *pool;
	struct pool_hdr_s *pool_entry;
	u32 i;

	pool = kmalloc(sizeof(*pool), GFP_ATOMIC);
	if (!pool)
		return NULL;

	memset(pool, 0, sizeof(*pool));
	pool->os = os;
	pool->use_lock = use_lock;

	/* Allocate an array where each array item is the size of a pool_hdr_s
	 * plus the requested memory item size (size)
	 */
	pool->a = efct_array_alloc(os, size + sizeof(struct pool_hdr_s),
				   count);
	if (!pool->a) {
		efct_pool_free(pool);
		return NULL;
	}

	INIT_LIST_HEAD(&pool->freelist);
	for (i = 0; i < count; i++) {
		pool_entry = (struct pool_hdr_s *)efct_array_get(pool->a, i);
		INIT_LIST_HEAD(&pool_entry->list_entry);
		list_add_tail(&pool_entry->list_entry, &pool->freelist);
	}

	if (pool->use_lock)
		spin_lock_init(&pool->lock);

	return pool;
}

/**
 * @brief Reset a memory pool.
 *
 * Place all pool elements on the free list, and zero them.
 *
 * @param pool Pointer to the pool object.
 *
 * @return None.
 */
void
efct_pool_reset(struct efct_pool_s *pool)
{
	u32 i;
	u32 count = efct_array_get_count(pool->a);
	u32 size = efct_array_get_size(pool->a);
	unsigned long flags = 0;
	struct pool_hdr_s *pool_entry;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);

	/*
	 * Remove all the entries from the free list, otherwise we will
	 * encountered linked list asserts when they are re-added.
	 */
	while (!list_empty(&pool->freelist)) {
		pool_entry = list_first_entry(&pool->freelist,
					      struct pool_hdr_s, list_entry);
		list_del(&pool_entry->list_entry);
	}

	/* Reset the free list */
	INIT_LIST_HEAD(&pool->freelist);

	/* Return all elements to the free list and zero the elements */
	for (i = 0; i < count; i++) {
		pool_entry = (struct pool_hdr_s *)efct_array_get(pool->a, i);
		memset(pool_entry, 0, size - sizeof(struct pool_hdr_s));
		INIT_LIST_HEAD(&pool_entry->list_entry);
		list_add_tail(&pool_entry->list_entry, &pool->freelist);
	}
	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
}

/**
 * @brief Free a previously allocated memory pool.
 *
 * The memory pool is freed.
 *
 * @param pool Pointer to memory pool.
 *
 * @return None.
 */
void
efct_pool_free(struct efct_pool_s *pool)
{
	if (pool) {
		if (pool->a)
			efct_array_free(pool->a);
		kfree(pool);
	}
}

/**
 * @brief Allocate a memory pool item
 *
 * A memory pool item is taken from the free list and returned.
 *
 * @param pool Pointer to memory pool.
 *
 * @return Pointer to allocated item, otherwise NULL if there are
 * no unallocated items.
 */
void *
efct_pool_get(struct efct_pool_s *pool)
{
	struct pool_hdr_s *h = NULL;
	void *item = NULL;
	unsigned long flags = 0;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);

	if (!list_empty(&pool->freelist)) {
		h = list_first_entry(&pool->freelist, struct pool_hdr_s,
				     list_entry);
	}

	if (h) {
		list_del(&h->list_entry);
		/*
		 * Return the array item address offset by the size of
		 * pool_hdr_s
		 */
		item = &h[1];
	}

	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
	return item;
}

/**
 * @brief free memory pool item
 *
 * A memory pool item is freed.
 *
 * @param pool Pointer to memory pool.
 * @param item Pointer to item to free.
 *
 * @return None.
 */
void
efct_pool_put(struct efct_pool_s *pool, void *item)
{
	struct pool_hdr_s *h;
	unsigned long flags = 0;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);

	/* Fetch the address of the array item, which is the item address
	 * negatively offset by size of pool_hdr_s (note the index of [-1]
	 */
	h = &((struct pool_hdr_s *)item)[-1];

	INIT_LIST_HEAD(&h->list_entry);
	list_add_tail(&h->list_entry, &pool->freelist);

	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
}

/**
 * @brief free memory pool item
 *
 * A memory pool item is freed to head of list.
 *
 * @param pool Pointer to memory pool.
 * @param item Pointer to item to free.
 *
 * @return None.
 */
void
efct_pool_put_head(struct efct_pool_s *pool, void *item)
{
	struct pool_hdr_s *h;
	unsigned long flags = 0;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);

	/* Fetch the address of the array item, which is the item address
	 * negatively offset by size of pool_hdr_s (note the index of [-1]
	 */
	h = &((struct pool_hdr_s *)item)[-1];

	INIT_LIST_HEAD(&h->list_entry);
	list_add(&h->list_entry, &pool->freelist);

	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
}

/**
 * @brief Return memory pool item count.
 *
 * Returns the allocated number of items.
 *
 * @param pool Pointer to memory pool.
 *
 * @return Returns count of allocated items.
 */
u32
efct_pool_get_count(struct efct_pool_s *pool)
{
	u32 count;
	unsigned long flags = 0;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);
	count = efct_array_get_count(pool->a);
	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
	return count;
}

/**
 * @brief Return item given an index.
 *
 * A pointer to a memory pool item is returned given an index.
 *
 * @param pool Pointer to memory pool.
 * @param idx Index.
 *
 * @return Returns pointer to item, or NULL if index is invalid.
 */
void *
efct_pool_get_instance(struct efct_pool_s *pool, u32 idx)
{
	struct pool_hdr_s *h = efct_array_get(pool->a, idx);

	if (!h)
		return NULL;
	return &h[1];
}

/**
 * @brief Return count of free objects in a pool.
 *
 * The number of objects on a pool's free list.
 *
 * @param pool Pointer to memory pool.
 *
 * @return Returns count of objects on free list.
 */
u32
efct_pool_get_freelist_count(struct efct_pool_s *pool)
{
	u32 count = 0;
	struct pool_hdr_s *item;
	unsigned long flags = 0;

	if (pool->use_lock)
		spin_lock_irqsave(&pool->lock, flags);

	list_for_each_entry(item, &pool->freelist, list_entry) {
		count++;
	}

	if (pool->use_lock)
		spin_unlock_irqrestore(&pool->lock, flags);
	return count;
}
