/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#ifndef _EFCT_HW_H
#define _EFCT_HW_H

#include "../libefc_sli/sli4.h"
#include "efct_utils.h"

/*
 * EFCT PCI IDs
 */
#define EFCT_VENDOR_ID			0x10df  /* Emulex */
#define EFCT_DEVICE_ID_LPE31004		0xe307  /* LightPulse 16Gb x 4
						 * FC (lancer-g6)
						 */
#define PCI_PRODUCT_EMULEX_LPE32002	0xe307  /* LightPulse 32Gb x 2
						 * FC (lancer-g6)
						 */
#define EFCT_DEVICE_ID_G7		0xf407	/* LightPulse 32Gb x 4
						 * FC (lancer-g7)
						 */

/*Define rq_threads seq cbuf size to 4K (equal to SLI RQ max length)*/
#define EFCT_RQTHREADS_MAX_SEQ_CBUF     4096

/*Default RQ entries len used by driver*/
#define EFCT_HW_RQ_ENTRIES_MIN		512
#define EFCT_HW_RQ_ENTRIES_DEF		1024
#define EFCT_HW_RQ_ENTRIES_MAX		4096

/*Defines the size of the RQ buffers used for each RQ*/
#define EFCT_HW_RQ_SIZE_HDR             128
#define EFCT_HW_RQ_SIZE_PAYLOAD         1024

/*Define the maximum number of multi-receive queues*/
#define EFCT_HW_MAX_MRQS		8

/*
 * Define count of when to set the WQEC bit in a submitted
 * WQE, causing a consummed/released completion to be posted.
 */
#define EFCT_HW_WQEC_SET_COUNT		32

/*Send frame timeout in seconds*/
#define EFCT_HW_SEND_FRAME_TIMEOUT	10

/*
 * FDT Transfer Hint value, reads greater than this value
 * will be segmented to implement fairness.   A value of zero disables
 * the feature.
 */
#define EFCT_HW_FDT_XFER_HINT                   8192

/*
 * HW asserts/verify
 */
extern void
_efct_hw_assert(const char *cond, const char *filename, int linenum);
extern int
_efct_hw_verify(const char *cond, const char *filename, int linenum);

#define efct_hw_assert(cond) \
	do { \
		if ((!(cond)))	\
			_efct_hw_assert(#cond, __FILE__, __LINE__); \
	} while (0)

#define efct_hw_verify(cond, ...) _efct_hw_verify(#cond, __FILE__, __LINE__)
#define efct_hw_verify_arg(cond) efct_hw_verify(cond, EFCT_HW_RTN_INVALID_ARG)

/*
 * HW completion loop control parameters.
 *
 * The HW completion loop must terminate periodically
 * to keep the OS happy.  The loop terminates when a predefined
 * time has elapsed, but to keep the overhead of
 * computing time down, the time is only checked after a
 * number of loop iterations has completed.
 *
 * EFCT_HW_TIMECHECK_ITERATIONS	 number of loop iterations
 * between time checks
 *
 */

#define EFCT_HW_TIMECHECK_ITERATIONS	100
#define EFCT_HW_MAX_NUM_MQ 1
#define EFCT_HW_MAX_NUM_RQ 32
#define EFCT_HW_MAX_NUM_EQ 16
#define EFCT_HW_MAX_NUM_WQ 32

#define OCE_HW_MAX_NUM_MRQ_PAIRS 16

#define EFCT_HW_MAX_WQ_CLASS	4
#define EFCT_HW_MAX_WQ_CPU	128

/*
 * A CQ will be assinged to each WQ
 * (CQ must have 2X entries of the WQ for abort
 * processing), plus a separate one for each RQ PAIR and one for MQ
 */
#define EFCT_HW_MAX_NUM_CQ \
	((EFCT_HW_MAX_NUM_WQ * 2) + 1 + (OCE_HW_MAX_NUM_MRQ_PAIRS * 2))
/*
 * Macros used to size the Q hash table.
 */
#define B2(x)   ((x) | ((x) >> 1))
#define B4(x)   (B2(x) | (B2(x) >> 2))
#define B8(x)   (B4(x) | (B4(x) >> 4))
#define B16(x)  (B8(x) | (B8(x) >> 8))
#define B32(x)  (B16(x) | (B16(x) >> 16))
#define B32_NEXT_POWER_OF_2(x)      (B32((x) - 1) + 1)

/*
 * Q hash - size is the maximum of all the queue sizes, rounded up to the next
 * power of 2
 */
#define EFCT_HW_Q_HASH_SIZE	\
	B32_NEXT_POWER_OF_2(EFCT_HW_MAX_NUM_CQ)

#define EFCT_HW_RQ_HEADER_SIZE	128
#define EFCT_HW_RQ_HEADER_INDEX	0

/**
 * @brief Options for efct_hw_command().
 */
enum {
	/**< command executes synchronously and busy-waits for completion */
	EFCT_CMD_POLL,
	/**< command executes asynchronously. Uses callback */
	EFCT_CMD_NOWAIT,
};

enum efct_hw_rtn_e {
	EFCT_HW_RTN_SUCCESS = 0,
	EFCT_HW_RTN_SUCCESS_SYNC = 1,
	EFCT_HW_RTN_ERROR = -1,
	EFCT_HW_RTN_NO_RESOURCES = -2,
	EFCT_HW_RTN_NO_MEMORY = -3,
	EFCT_HW_RTN_IO_NOT_ACTIVE = -4,
	EFCT_HW_RTN_IO_ABORT_IN_PROGRESS = -5,
	EFCT_HW_RTN_IO_PORT_OWNED_ALREADY_ABORTED = -6,
	EFCT_HW_RTN_INVALID_ARG = -7,
};

#define EFCT_HW_RTN_IS_ERROR(e)	((e) < 0)

enum efct_hw_reset_e {
	EFCT_HW_RESET_FUNCTION,
	EFCT_HW_RESET_FIRMWARE,
	EFCT_HW_RESET_MAX
};

enum efct_hw_property_e {
	EFCT_HW_N_IO,
	EFCT_HW_N_SGL,
	EFCT_HW_MAX_IO,
	EFCT_HW_MAX_SGE,
	EFCT_HW_MAX_SGL,
	EFCT_HW_MAX_NODES,
	EFCT_HW_MAX_RQ_ENTRIES,
	EFCT_HW_TOPOLOGY,	/**< auto, nport, loop */
	EFCT_HW_WWN_NODE,
	EFCT_HW_WWN_PORT,
	EFCT_HW_FW_REV,
	EFCT_HW_FW_REV2,
	EFCT_HW_IPL,
	EFCT_HW_VPD,
	EFCT_HW_VPD_LEN,
	EFCT_HW_MODE,		/**< initiator, target, both */
	EFCT_HW_LINK_SPEED,
	EFCT_HW_IF_TYPE,
	EFCT_HW_SLI_REV,
	EFCT_HW_SLI_FAMILY,
	EFCT_HW_RQ_PROCESS_LIMIT,
	EFCT_HW_RQ_DEFAULT_BUFFER_SIZE,
	EFCT_HW_AUTO_XFER_RDY_CAPABLE,
	EFCT_HW_AUTO_XFER_RDY_XRI_CNT,
	EFCT_HW_AUTO_XFER_RDY_SIZE,
	EFCT_HW_AUTO_XFER_RDY_BLK_SIZE,
	EFCT_HW_AUTO_XFER_RDY_T10_ENABLE,
	EFCT_HW_AUTO_XFER_RDY_P_TYPE,
	EFCT_HW_AUTO_XFER_RDY_REF_TAG_IS_LBA,
	EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALID,
	EFCT_HW_AUTO_XFER_RDY_APP_TAG_VALUE,
	EFCT_HW_DIF_CAPABLE,
	EFCT_HW_DIF_SEED,
	EFCT_HW_DIF_MODE,
	EFCT_HW_DIF_MULTI_SEPARATE,
	EFCT_HW_DUMP_MAX_SIZE,
	EFCT_HW_DUMP_READY,
	EFCT_HW_DUMP_PRESENT,
	EFCT_HW_RESET_REQUIRED,
	EFCT_HW_FW_ERROR,
	EFCT_HW_FW_READY,
	EFCT_HW_HIGH_LOGIN_MODE,
	EFCT_HW_PREREGISTER_SGL,
	EFCT_HW_HW_REV1,
	EFCT_HW_HW_REV2,
	EFCT_HW_HW_REV3,
	EFCT_HW_LINKCFG,
	EFCT_HW_ETH_LICENSE,
	EFCT_HW_LINK_MODULE_TYPE,
	EFCT_HW_NUM_CHUTES,
	EFCT_HW_WAR_VERSION,
	EFCT_HW_DISABLE_AR_TGT_DIF,
	/**< emulate IAAB=0 for initiator-commands only */
	EFCT_HW_EMULATE_I_ONLY_AAB,
	/**< enable driver timeouts for target WQEs */
	EFCT_HW_EMULATE_TARGET_WQE_TIMEOUT,
	EFCT_HW_LINK_CONFIG_SPEED,
	EFCT_HW_CONFIG_TOPOLOGY,
	EFCT_HW_BOUNCE,
	EFCT_HW_PORTNUM,
	EFCT_HW_BIOS_VERSION_STRING,
	EFCT_HW_RQ_SELECT_POLICY,
	EFCT_HW_SGL_CHAINING_CAPABLE,
	EFCT_HW_SGL_CHAINING_ALLOWED,
	EFCT_HW_SGL_CHAINING_HOST_ALLOCATED,
	EFCT_HW_SEND_FRAME_CAPABLE,
	EFCT_HW_RQ_SELECTION_POLICY,
	EFCT_HW_RR_QUANTA,
	EFCT_HW_FILTER_DEF,
	EFCT_HW_MAX_VPORTS,
	EFCT_ESOC,
};

enum {
	EFCT_HW_TOPOLOGY_AUTO,
	EFCT_HW_TOPOLOGY_NPORT,
	EFCT_HW_TOPOLOGY_LOOP,
	EFCT_HW_TOPOLOGY_NONE,
	EFCT_HW_TOPOLOGY_MAX
};

enum {
	EFCT_HW_MODE_INITIATOR,
	EFCT_HW_MODE_TARGET,
	EFCT_HW_MODE_BOTH,
	EFCT_HW_MODE_MAX
};

/**
 * @brief Port protocols
 */

enum efct_hw_port_protocol_e {
	EFCT_HW_PORT_PROTOCOL_FCOE,
	EFCT_HW_PORT_PROTOCOL_FC,
	EFCT_HW_PORT_PROTOCOL_OTHER,
};

#define EFCT_HW_MAX_PROFILES	40
/**
 * @brief A Profile Descriptor
 */
struct efct_hw_profile_descriptor_s {
	u32	profile_index;
	u32	profile_id;
	char		profile_description[512];
};

/**
 * @brief A Profile List
 */
struct efct_hw_profile_list_s {
	u32			num_descriptors;
	struct efct_hw_profile_descriptor_s
			descriptors[EFCT_HW_MAX_PROFILES];
};

/**
 * HW workarounds
 *
 * This API contains the declarations that allow
 * run-time selection of workarounds
 * based on the asic type and revision, and range of fw revision.
 */

/**
 * @brief pack fw revision values into a single uint64_t
 */

/* Two levels of macro needed due to expansion */
#define HW_FWREV(a, b, c, d) (((uint64_t)(a) << 48) | ((uint64_t)(b) << 32)\
			| ((uint64_t)(c) << 16) | ((uint64_t)(d)))

#define EFCT_FW_VER_STR(a, b, c, d) (#a "." #b "." #c "." #d)

struct efct_hw_workaround_s {
	u64 fwrev;

	/* Control Declarations here ...*/

	u8 retain_tsend_io_length;

	/* Use unregistered RPI */
	bool use_unregistered_rpi;
	u32 unregistered_rid;
	u32 unregistered_index;

	u8 disable_ar_tgt_dif; /* Disable auto rsp if target DIF */
	bool disable_dump_loc;
	bool use_dif_quarantine;
	bool use_dif_sec_xri;

	bool override_fcfi;

	bool fw_version_too_low;

	bool sglc_misreported;

	u8 ignore_send_frame;

};

/**
 * @brief Defines DIF operation modes
 */
enum {
	EFCT_HW_DIF_MODE_INLINE,
	EFCT_HW_DIF_MODE_SEPARATE,
};

/**
 * @brief T10 DIF operations.
 */
enum efct_hw_dif_oper_e {
	EFCT_HW_DIF_OPER_DISABLED,
	EFCT_HW_SGE_DIFOP_INNODIFOUTCRC,
	EFCT_HW_SGE_DIFOP_INCRCOUTNODIF,
	EFCT_HW_SGE_DIFOP_INNODIFOUTCHKSUM,
	EFCT_HW_SGE_DIFOP_INCHKSUMOUTNODIF,
	EFCT_HW_SGE_DIFOP_INCRCOUTCRC,
	EFCT_HW_SGE_DIFOP_INCHKSUMOUTCHKSUM,
	EFCT_HW_SGE_DIFOP_INCRCOUTCHKSUM,
	EFCT_HW_SGE_DIFOP_INCHKSUMOUTCRC,
	EFCT_HW_SGE_DIFOP_INRAWOUTRAW,
};

#define EFCT_HW_DIF_OPER_PASS_THRU	EFCT_HW_SGE_DIFOP_INCRCOUTCRC
#define EFCT_HW_DIF_OPER_STRIP		EFCT_HW_SGE_DIFOP_INCRCOUTNODIF
#define EFCT_HW_DIF_OPER_INSERT		EFCT_HW_SGE_DIFOP_INNODIFOUTCRC

/**
 * @brief T10 DIF block sizes.
 */
enum efct_hw_dif_blk_size_e {
	EFCT_HW_DIF_BK_SIZE_512,
	EFCT_HW_DIF_BK_SIZE_1024,
	EFCT_HW_DIF_BK_SIZE_2048,
	EFCT_HW_DIF_BK_SIZE_4096,
	EFCT_HW_DIF_BK_SIZE_520,
	EFCT_HW_DIF_BK_SIZE_4104,
	EFCT_HW_DIF_BK_SIZE_NA = 0
};

/**
 * @brief Link configurations.
 */
enum efct_hw_linkcfg_e {
	EFCT_HW_LINKCFG_4X10G = 0,
	EFCT_HW_LINKCFG_1X40G,
	EFCT_HW_LINKCFG_2X16G,
	EFCT_HW_LINKCFG_4X8G,
	EFCT_HW_LINKCFG_4X1G,
	EFCT_HW_LINKCFG_2X10G,
	EFCT_HW_LINKCFG_2X10G_2X8G,

	/* must be last */
	EFCT_HW_LINKCFG_NA,
};

/**
 * @brief link module types
 *
 * (note: these just happen to match SLI4 values)
 */

enum {
	EFCT_HW_LINK_MODULE_TYPE_1GB = 0x0004,
	EFCT_HW_LINK_MODULE_TYPE_2GB = 0x0008,
	EFCT_HW_LINK_MODULE_TYPE_4GB = 0x0040,
	EFCT_HW_LINK_MODULE_TYPE_8GB = 0x0080,
	EFCT_HW_LINK_MODULE_TYPE_10GB = 0x0100,
	EFCT_HW_LINK_MODULE_TYPE_16GB = 0x0200,
	EFCT_HW_LINK_MODULE_TYPE_32GB = 0x0400,
};

/**
 * @brief T10 DIF information passed to the transport.
 */

struct efct_hw_dif_info_s {
	enum efct_hw_dif_oper_e dif_oper;
	enum efct_hw_dif_blk_size_e blk_size;
	u32 ref_tag_cmp;
	u32 ref_tag_repl;
	u16 app_tag_cmp;
	u16 app_tag_repl;
	bool check_ref_tag;
	bool check_app_tag;
	bool check_guard;
	bool auto_incr_ref_tag;
	bool repl_app_tag;
	bool repl_ref_tag;
	bool dif_separate;

	/* If the APP TAG is 0xFFFF, disable REF TAG and CRC field chk */
	bool disable_app_ffff;

	/* if the APP TAG is 0xFFFF and REF TAG is 0xFFFF_FFFF,
	 * disable checking the received CRC field.
	 */
	bool disable_app_ref_ffff;
	u16 dif_seed;
	u8 dif;
};

enum efct_hw_io_type_e {
	EFCT_HW_ELS_REQ,	/**< ELS request */
	EFCT_HW_ELS_RSP,	/**< ELS response */
	EFCT_HW_ELS_RSP_SID,	/**< ELS response, override the S_ID */
	EFCT_HW_FC_CT,		/**< FC Common Transport */
	EFCT_HW_FC_CT_RSP,	/**< FC Common Transport Response */
	EFCT_HW_BLS_ACC,	/**< BLS accept (BA_ACC) */
	EFCT_HW_BLS_ACC_SID,	/**< BLS accept (BA_ACC), override the S_ID */
	EFCT_HW_BLS_RJT,	/**< BLS reject (BA_RJT) */
	EFCT_HW_IO_TARGET_READ,
	EFCT_HW_IO_TARGET_WRITE,
	EFCT_HW_IO_TARGET_RSP,
	EFCT_HW_IO_INITIATOR_READ,
	EFCT_HW_IO_INITIATOR_WRITE,
	EFCT_HW_IO_INITIATOR_NODATA,
	EFCT_HW_IO_DNRX_REQUEUE,
	EFCT_HW_IO_MAX,
};

enum efct_hw_io_state_e {
	EFCT_HW_IO_STATE_FREE,
	EFCT_HW_IO_STATE_INUSE,
	EFCT_HW_IO_STATE_WAIT_FREE,
	EFCT_HW_IO_STATE_WAIT_SEC_HIO,
};

/* Descriptive strings for the HW IO request types (note: these must always
 * match up with the enum efct_hw_io_type_e declaration)
 **/
#define EFCT_HW_IO_TYPE_STRINGS \
	"ELS request", \
	"ELS response", \
	"ELS response(set SID)", \
	"FC CT request", \
	"BLS accept", \
	"BLS accept(set SID)", \
	"BLS reject", \
	"target read", \
	"target write", \
	"target response", \
	"initiator read", \
	"initiator write", \
	"initiator nodata",

struct efct_hw_s;
/**
 * @brief HW command context.
 *
 * Stores the state for the asynchronous commands sent to the hardware.
 */
struct efct_command_ctx_s {
	struct list_head	list_entry;
	/**< Callback function */
	int	(*cb)(struct efct_hw_s *hw, int status, u8 *mqe, void *arg);
	void	*arg;	/**< Argument for callback */
	u8	*buf;	/**< buffer holding command / results */
	void	*ctx;	/**< upper layer context */
};

struct efct_hw_sgl_s {
	uintptr_t	addr;
	size_t		len;
};

union efct_hw_io_param_u {
	struct {
		__be16	 ox_id;
		__be16	 rx_id;
		u8  payload[12];	/**< big enough for ABTS BA_ACC */
	} bls;
	struct {
		u32 s_id;
		u16 ox_id;
		u16 rx_id;
		u8  payload[12];	/**< big enough for ABTS BA_ACC */
	} bls_sid;
	struct {
		u8	r_ctl;
		u8	type;
		u8	df_ctl;
		u8 timeout;
	} bcast;
	struct {
		u16 ox_id;
		u8 timeout;
	} els;
	struct {
		u32 s_id;
		u16 ox_id;
		u8 timeout;
	} els_sid;
	struct {
		u8	r_ctl;
		u8	type;
		u8	df_ctl;
		u8 timeout;
	} fc_ct;
	struct {
		u8	r_ctl;
		u8	type;
		u8	df_ctl;
		u8 timeout;
		u16 ox_id;
	} fc_ct_rsp;
	struct {
		u32 offset;
		u16 ox_id;
		u16 flags;
		u8	cs_ctl;
		enum efct_hw_dif_oper_e dif_oper;
		enum efct_hw_dif_blk_size_e blk_size;
		u8	timeout;
		u32 app_id;
	} fcp_tgt;
	struct {
		struct efc_dma_s	*cmnd;
		struct efc_dma_s	*rsp;
		enum efct_hw_dif_oper_e dif_oper;
		enum efct_hw_dif_blk_size_e blk_size;
		u32	cmnd_size;
		u16	flags;
		u8		timeout;
		u32	first_burst;
	} fcp_ini;
};

/**
 * @brief WQ steering mode
 */
enum efct_hw_wq_steering_e {
	EFCT_HW_WQ_STEERING_CLASS,
	EFCT_HW_WQ_STEERING_REQUEST,
	EFCT_HW_WQ_STEERING_CPU,
};

/**
 * @brief HW wqe object
 */
struct efct_hw_wqe_s {
	struct list_head	list_entry;
	/**< set if abort wqe needs to be submitted */
	bool		abort_wqe_submit_needed;
	/**< set to 1 to have hardware to automatically send ABTS */
	bool		send_abts;
	/**< TRUE if DNRX was set on this IO */
	bool		auto_xfer_rdy_dnrx;
	u32	id;
	u32	abort_reqtag;
	/**< work queue entry buffer */
	u8		*wqebuf;
};

/**
 * @brief HW IO object.
 *
 * Stores the per-IO information necessary
 * for both the lower (SLI) and upper
 * layers (efct).
 */
struct efct_hw_io_s {
	/* Owned by HW */

	/* reference counter and callback function */
	struct kref ref;
	void (*release)(struct kref *arg);
	/**< used for busy, wait_free, free lists */
	struct list_head	list_entry;
	/**< used for timed_wqe list */
	struct list_head	wqe_link;
	/**< used for io posted dnrx list */
	struct list_head	dnrx_link;
	/**< state of IO: free, busy, wait_free */
	enum efct_hw_io_state_e state;
	/**< Work queue object, with link for pending */
	struct efct_hw_wqe_s	wqe;
	/**< Lock to synchronize TRSP and AXT Data/Cmd Cqes */
	spinlock_t	axr_lock;
	/**< pointer back to hardware context */
	struct efct_hw_s	*hw;
	struct efc_remote_node_s	*rnode;
	struct efct_hw_auto_xfer_rdy_buffer_s *axr_buf;
	struct efc_dma_s	xfer_rdy;
	u16	type;
	/**< IO abort count */
	u32	port_owned_abort_count;
	/**< WQ assigned to the exchange */
	struct hw_wq_s	*wq;
	 /**< Exchange is active in FW */
	bool		xbusy;
	/**< Function called on IO completion */
	int
	(*done)(struct efct_hw_io_s *hio,
		struct efc_remote_node_s *rnode,
			u32 len, int status,
			u32 ext, void *ul_arg);
	/**< argument passed to "IO done" callback */
	void		*arg;
	/**< Function called on abort completion */
	int
	(*abort_done)(struct efct_hw_io_s *hio,
		      struct efc_remote_node_s *rnode,
			u32 len, int status,
			u32 ext, void *ul_arg);
	/**< argument passed to "abort done" callback */
	void		*abort_arg;
	/**< needed for bug O127585: length of IO */
	size_t		length;
	/**< timeout value for target WQEs */
	u8		tgt_wqe_timeout;
	/**< timestamp when current WQE was submitted */
	u64	submit_ticks;

	/**< if TRUE, latched status shld be returned */
	bool		status_saved;
	/**< if TRUE, abort is in progress */
	bool		abort_in_progress;
	bool		quarantine;	/**< set if IO to be quarantined */
	/**< set if first phase of IO */
	bool		quarantine_first_phase;
	/**< set if POST_XRI used to send XRI to chip */
	bool		is_port_owned;
	/**< TRUE if DNRX was set on this IO */
	bool		auto_xfer_rdy_dnrx;
	u32	saved_status;	/**< latched status */
	u32	saved_len;	/**< latched length */
	u32	saved_ext;	/**< latched extended status */

	/**< EQ that this HIO came up on */
	struct hw_eq_s	*eq;
	/**< WQ steering mode request */
	enum efct_hw_wq_steering_e	wq_steering;
	/**< WQ class if steering mode is Class */
	u8		wq_class;

	/*  Owned by SLI layer */
	u16	reqtag;		/**< request tag for this HW IO */
	/**< request tag for an abort of this HW IO
	 * (note: this is a 32 bit value
	 * to allow us to use UINT32_MAX as an uninitialized value)
	 **/
	u32	abort_reqtag;
	u32	indicator;	/**< XRI */
	struct efc_dma_s	def_sgl;/**< default scatter gather list */
	u32	def_sgl_count;	/**< count of SGEs in default SGL */
	struct efc_dma_s	*sgl;	/**< pointer to current active SGL */
	u32	sgl_count;	/**< count of SGEs in io->sgl */
	u32	first_data_sge;	/**< index of first data SGE */
	struct efc_dma_s	*ovfl_sgl;	/**< overflow SGL */
	u32	ovfl_sgl_count;	/**< count of SGEs in default SGL */
	 /**< pointer to overflow segment len */
	struct sli4_lsp_sge_s	*ovfl_lsp;
	u32	n_sge;		/**< number of active SGEs */
	u32	sge_offset;

	 /**< Secondary HW IO context */
	struct efct_hw_io_s	*sec_hio;
	/**< Secondary HW IO context saved iparam */
	union efct_hw_io_param_u sec_iparam;
	/**< Secondary HW IO context saved len */
	u32	sec_len;

	/* Owned by upper layer */
	/**< where upper layer can store ref to its IO */
	void		*ul_io;
};

/**
 * @brief HW callback type
 *
 * Typedef for HW "done" callback.
 */
typedef int (*efct_hw_done_t)(struct efct_hw_io_s *, struct efc_remote_node_s *,
			      u32 len, int status, u32 ext, void *ul_arg);

enum efct_hw_port_e {
	EFCT_HW_PORT_INIT,
	EFCT_HW_PORT_SHUTDOWN,
	EFCT_HW_PORT_SET_LINK_CONFIG,
};

/**
 * @brief Node group rpi reference
 */
struct efct_hw_rpi_ref_s {
	atomic_t rpi_count;
	atomic_t rpi_attached;
};

/**
 * @brief HW link stat types
 */
enum efct_hw_link_stat_e {
	EFCT_HW_LINK_STAT_LINK_FAILURE_COUNT,
	EFCT_HW_LINK_STAT_LOSS_OF_SYNC_COUNT,
	EFCT_HW_LINK_STAT_LOSS_OF_SIGNAL_COUNT,
	EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_COUNT,
	EFCT_HW_LINK_STAT_INVALID_XMIT_WORD_COUNT,
	EFCT_HW_LINK_STAT_CRC_COUNT,
	EFCT_HW_LINK_STAT_PRIMITIVE_SEQ_TIMEOUT_COUNT,
	EFCT_HW_LINK_STAT_ELASTIC_BUFFER_OVERRUN_COUNT,
	EFCT_HW_LINK_STAT_ARB_TIMEOUT_COUNT,
	EFCT_HW_LINK_STAT_ADVERTISED_RCV_B2B_CREDIT,
	EFCT_HW_LINK_STAT_CURR_RCV_B2B_CREDIT,
	EFCT_HW_LINK_STAT_ADVERTISED_XMIT_B2B_CREDIT,
	EFCT_HW_LINK_STAT_CURR_XMIT_B2B_CREDIT,
	EFCT_HW_LINK_STAT_RCV_EOFA_COUNT,
	EFCT_HW_LINK_STAT_RCV_EOFDTI_COUNT,
	EFCT_HW_LINK_STAT_RCV_EOFNI_COUNT,
	EFCT_HW_LINK_STAT_RCV_SOFF_COUNT,
	EFCT_HW_LINK_STAT_RCV_DROPPED_NO_AER_COUNT,
	EFCT_HW_LINK_STAT_RCV_DROPPED_NO_RPI_COUNT,
	EFCT_HW_LINK_STAT_RCV_DROPPED_NO_XRI_COUNT,
	EFCT_HW_LINK_STAT_MAX,		/**< must be last */
};

enum efct_hw_host_stat_e {
	EFCT_HW_HOST_STAT_TX_KBYTE_COUNT,
	EFCT_HW_HOST_STAT_RX_KBYTE_COUNT,
	EFCT_HW_HOST_STAT_TX_FRAME_COUNT,
	EFCT_HW_HOST_STAT_RX_FRAME_COUNT,
	EFCT_HW_HOST_STAT_TX_SEQ_COUNT,
	EFCT_HW_HOST_STAT_RX_SEQ_COUNT,
	EFCT_HW_HOST_STAT_TOTAL_EXCH_ORIG,
	EFCT_HW_HOST_STAT_TOTAL_EXCH_RESP,
	EFCT_HW_HOSY_STAT_RX_P_BSY_COUNT,
	EFCT_HW_HOST_STAT_RX_F_BSY_COUNT,
	EFCT_HW_HOST_STAT_DROP_FRM_DUE_TO_NO_RQ_BUF_COUNT,
	EFCT_HW_HOST_STAT_EMPTY_RQ_TIMEOUT_COUNT,
	EFCT_HW_HOST_STAT_DROP_FRM_DUE_TO_NO_XRI_COUNT,
	EFCT_HW_HOST_STAT_EMPTY_XRI_POOL_COUNT,
	EFCT_HW_HOST_STAT_MAX /* MUST BE LAST */
};

enum efct_hw_state_e {
	EFCT_HW_STATE_UNINITIALIZED,	/* power-on, no allefct, no init */
	EFCT_HW_STATE_QUEUES_ALLOCATED,	/* chip is reset, alloc are cmpl
					 *(queues not registered)
					 */
	EFCT_HW_STATE_ACTIVE,		/* chip is up an running */
	EFCT_HW_STATE_RESET_IN_PROGRESS,/* chip is being reset */
	EFCT_HW_STATE_TEARDOWN_IN_PROGRESS,/* teardown has been started */
};

/**
 * @brief Structure to track optimized write buffers posted
 * to chip owned XRIs.
 *
 * Note:
 *	The rqindex will be set the following "fake" indexes.
 *	This will be used when the buffer is returned via
 *	efct_seq_free() to make the buffer available
 *	for re-use on another XRI.
 *
 *	The dma->alloc pointer on the dummy header will be used to
 *	get back to this structure when the buffer is freed.
 *
 *	More of these object may be allocated on the fly if more XRIs
 *	are pushed to the chip.
 */
#define EFCT_HW_RQ_INDEX_DUMMY_HDR	0xFF00
#define EFCT_HW_RQ_INDEX_DUMMY_DATA	0xFF01
struct efct_hw_auto_xfer_rdy_buffer_s {
	struct fc_frame_header hdr;	/* < used to build a dummy
					 * data hdr for unsol processing **/
	struct efc_hw_rq_buffer_s header;	/**< Points to dummy data hdr */
	struct efc_hw_rq_buffer_s payload;	/**< rcvd frame payload buff */
	struct efc_hw_sequence_s seq;         /**< seq for passing the buffs */
	u8 data_cqe;
	u8 cmd_cqe;

	/* fields saved from cmd hdr that are needed when data arrives */
	u8 fcfi;

	/* To handle outof order completions save AXR cmd and data cqes */
	u8 call_axr_cmd;
	u8 call_axr_data;
	struct efc_hw_sequence_s *cmd_seq;
};

/**
 * @brief Node group rpi reference
 */
struct efct_hw_link_stat_counts_s {
	u8 overflow;
	u32 counter;
};

/**
 * @brief HW object describing fc host stats
 */
struct efct_hw_host_stat_counts_s {
	u32 counter;
};

#define TID_HASH_BITS	8
#define TID_HASH_LEN	BIT(TID_HASH_BITS)

enum hw_cq_handler_e {
	HW_CQ_HANDLER_LOCAL,
	HW_CQ_HANDLER_THREAD,
};

#include "efct_hw_queues.h"

/**
 * @brief Structure used for the hash lookup of queue IDs
 */
struct efct_queue_hash_s {
	bool in_use;
	u16 id;
	u16 index;
};

/**
 * @brief Define the fields required to implement DIF quarantine.
 */
#define EFCT_HW_QUARANTINE_QUEUE_DEPTH	4

struct efct_quarantine_info_s {
	u32	quarantine_index;
	struct efct_hw_io_s *quarantine_ios[EFCT_HW_QUARANTINE_QUEUE_DEPTH];
};

/**
 * @brief Define the WQ callback object
 */
struct hw_wq_callback_s {
	u16 instance_index;	/**< use for request tag */
	void (*callback)(void *arg, u8 *cqe, int status);
	void *arg;
};

struct efct_hw_config {
	u32	n_eq; /**< number of event queues */
	u32	n_cq; /**< number of completion queues */
	u32	n_mq; /**< number of mailbox queues */
	u32	n_rq; /**< number of receive queues */
	u32	n_wq; /**< number of work queues */
	u32	n_io; /**< total number of IO objects */
	u32	n_sgl;/**< length of SGL */
	u32	speed;	/** requested link speed in Mbps */
	u32	topology;  /** requested link topology */
	/** size of the buffers for first burst */
	u32	rq_default_buffer_size;
	/** Initial XRIs to post to chip at init */
	u32	auto_xfer_rdy_xri_cnt;
	/** max size IO to use with this feature */
	u32	auto_xfer_rdy_size;
	/** block size to use with this feature */
	u8		auto_xfer_rdy_blk_size_chip;
	u8         esoc;
	/** The seed for the DIF CRC calculation */
	u16	dif_seed;
	u16	auto_xfer_rdy_app_tag_value;
	u8		dif_mode; /**< DIF mode to use */
	/** Enable initiator-only auto-abort */
	bool		i_only_aab;
	/** Enable driver target wqe timeouts */
	u8		emulate_tgt_wqe_timeout;
	bool		bounce;
	/**< Queue topology string */
	const char	*queue_topology;
	/** Enable t10 PI for auto xfer ready */
	u8		auto_xfer_rdy_t10_enable;
	/** p_type for auto xfer ready */
	u8		auto_xfer_rdy_p_type;
	u8		auto_xfer_rdy_ref_tag_is_lba;
	u8		auto_xfer_rdy_app_tag_valid;
	/** MRQ RQ selection policy */
	u8		rq_selection_policy;
	 /** RQ quanta if rq_selection_policy == 2 */
	u8		rr_quanta;
	u32	filter_def[SLI4_CMD_REG_FCFI_NUM_RQ_CFG];
};

/**
 * @brief HW object
 */
struct efct_hw_s {
	void		*os;
	struct sli4_s		sli;
	u16	ulp_start;
	u16	ulp_max;
	u32	dump_size;
	enum efct_hw_state_e state;
	bool		hw_setup_called;
	u8		sliport_healthcheck;
	u16        watchdog_timeout;

	/** HW configuration, subject to efct_hw_set()  */
	struct efct_hw_config config;

	/* calculated queue sizes for each type */
	u32	num_qentries[SLI_QTYPE_MAX];

	/* Storage for SLI queue objects */
	struct sli4_queue_s	wq[EFCT_HW_MAX_NUM_WQ];
	struct sli4_queue_s	rq[EFCT_HW_MAX_NUM_RQ];
	u16	hw_rq_lookup[EFCT_HW_MAX_NUM_RQ];
	struct sli4_queue_s	mq[EFCT_HW_MAX_NUM_MQ];
	struct sli4_queue_s	cq[EFCT_HW_MAX_NUM_CQ];
	struct sli4_queue_s	eq[EFCT_HW_MAX_NUM_EQ];

	/* HW queue */
	u32	eq_count;
	u32	cq_count;
	u32	mq_count;
	u32	wq_count;
	u32	rq_count;			/**< count of SLI RQs */
	struct list_head	eq_list;

	struct efct_queue_hash_s cq_hash[EFCT_HW_Q_HASH_SIZE];
	struct efct_queue_hash_s rq_hash[EFCT_HW_Q_HASH_SIZE];
	struct efct_queue_hash_s wq_hash[EFCT_HW_Q_HASH_SIZE];

	/* Storage for HW queue objects */
	struct hw_wq_s	*hw_wq[EFCT_HW_MAX_NUM_WQ];
	struct hw_rq_s	*hw_rq[EFCT_HW_MAX_NUM_RQ];
	struct hw_mq_s	*hw_mq[EFCT_HW_MAX_NUM_MQ];
	struct hw_cq_s	*hw_cq[EFCT_HW_MAX_NUM_CQ];
	struct hw_eq_s	*hw_eq[EFCT_HW_MAX_NUM_EQ];
	/**< count of hw_rq[] entries */
	u32	hw_rq_count;
	/**< count of multirq RQs */
	u32	hw_mrq_count;

	 /**< pool per class WQs */
	struct efct_varray_s	*wq_class_array[EFCT_HW_MAX_WQ_CLASS];
	/**< pool per CPU WQs */
	struct efct_varray_s	*wq_cpu_array[EFCT_HW_MAX_WQ_CPU];

	/* Sequence objects used in incoming frame processing */
	struct efct_array_s	*seq_pool;

	/* Auto XFER RDY Buffers - protect with io_lock */
	/**< TRUE if auto xfer rdy enabled */
	bool auto_xfer_rdy_enabled;

	/**< pool of efct_hw_auto_xfer_rdy_buffer_t objects **/
	struct efct_pool_s *auto_xfer_rdy_buf_pool;

	/** Maintain an ordered, linked list of outstanding HW commands. */
	spinlock_t	cmd_lock;
	struct list_head	cmd_head;
	struct list_head	cmd_pending;
	u32	cmd_head_count;

	struct sli4_link_event_s link;
	/**< link configuration setting */
	enum efct_hw_linkcfg_e linkcfg;
	 /**< Ethernet license; to enable FCoE on Lancer */
	u32 eth_license;

	/* EFCT domain objects index by FCFI */
	int		first_domain_idx; /* Wrkarnd for srb->fcfi == 0 */
	struct efc_domain_s	*domains[SLI4_MAX_FCFI];

	/* Table of FCFI values index by FCF_index */
	u16	fcf_index_fcfi[SLI4_MAX_FCF_INDEX];

	u16	fcf_indicator;

	/**< pointer array of IO objects */
	struct efct_hw_io_s	**io;
	/**< array of WQE buffs mapped to IO objects */
	u8		*wqe_buffs;

	/**< IO lock to synchronize list access */
	spinlock_t	io_lock;
	/**< IO lock to synchronize IO aborting */
	spinlock_t	io_abort_lock;
	/**< List of IO objects in use */
	struct list_head	io_inuse;
	/**< List of IO objects with a timed target WQE */
	struct list_head	io_timed_wqe;
	/**< List of IO objects waiting to be freed */
	struct list_head	io_wait_free;
	/**< List of IO objects available for allocation */
	struct list_head	io_free;
	/**< List of IO objects posted for chip use */
	struct list_head	io_port_owned;
	/**< List of IO objects needing auto xfer rdy buffers */
	struct list_head	io_port_dnrx;

	struct efc_dma_s	loop_map;

	struct efc_dma_s	xfer_rdy;

	struct efc_dma_s	dump_sges;

	struct efc_dma_s	rnode_mem;

	struct efct_hw_rpi_ref_s *rpi_ref;

	char		*hw_war_version;
	struct efct_hw_workaround_s workaround;

	atomic_t io_alloc_failed_count;

	/* Secondary HW IO context wait list */
	struct list_head	sec_hio_wait_list;
	/* Workaround: Count of IOs that were put on the
	 * Secondary HW IO wait list pointer to queue topology
	 */
	u32	sec_hio_wait_count;

#define HW_MAX_TCMD_THREADS		16
	struct efct_hw_qtop_s	*qtop;		/* pointer to queue topology */

	 /**< stat: wq sumbit count */
	u32	tcmd_wq_submit[EFCT_HW_MAX_NUM_WQ];
	/**< stat: wq complete count */
	u32	tcmd_wq_complete[EFCT_HW_MAX_NUM_WQ];

	struct timer_list	wqe_timer;	/**< Timer to periodically
						  *check for WQE timeouts
						  **/
	struct timer_list	watchdog_timer;	/**< Timer for heartbeat */
	bool	in_active_wqe_timer;		/* < TRUE if currently in
						 * active wqe timer handler
						 */
	bool	active_wqe_timer_shutdown;	/* TRUE if wqe
						 * timer is to be shutdown
						 */

	struct list_head	iopc_list;	/*< list of IO
						 *processing contexts
						 */
	spinlock_t	iopc_list_lock;		/**< lock for iopc_list */

	struct efct_pool_s	*wq_reqtag_pool; /* < pool of
						  * struct hw_wq_callback_s obj
						  */

	atomic_t	send_frame_seq_id;	/* < send frame
						 * sequence ID
						 */
};

enum efct_hw_io_count_type_e {
	EFCT_HW_IO_INUSE_COUNT,
	EFCT_HW_IO_FREE_COUNT,
	EFCT_HW_IO_WAIT_FREE_COUNT,
	EFCT_HW_IO_PORT_OWNED_COUNT,
	EFCT_HW_IO_N_TOTAL_IO_COUNT,
};

extern void
(*tcmd_cq_handler)(struct efct_hw_s *hw, u32 cq_idx,
		   void *cq_handler_arg);
/*
 * HW queue data structures
 */

struct hw_eq_s {
	struct list_head	list_entry;
	enum sli4_qtype_e type;		/**< must be second */
	u32 instance;
	u32 entry_count;
	u32 entry_size;
	struct efct_hw_s *hw;
	struct sli4_queue_s *queue;
	struct list_head cq_list;
	u32 use_count;
	struct efct_varray_s *wq_array;		/*<< array of WQs */
};

struct hw_cq_s {
	struct list_head list_entry;
	enum sli4_qtype_e type;		/**< must be second */
	u32 instance;		/*<< CQ instance (cq_idx) */
	u32 entry_count;		/*<< Number of entries */
	u32 entry_size;		/*<< entry size */
	struct hw_eq_s *eq;			/*<< parent EQ */
	struct sli4_queue_s *queue;		/**< pointer to SLI4 queue */
	struct list_head q_list;	/**< list of children queues */
	u32 use_count;
};

void hw_thread_cq_handler(struct efct_hw_s *hw, struct hw_cq_s *cq);

struct hw_q_s {
	struct list_head list_entry;
	enum sli4_qtype_e type;		/*<< must be second */
};

struct hw_mq_s {
	struct list_head list_entry;
	enum sli4_qtype_e type;		/*<< must be second */
	u32 instance;

	u32 entry_count;
	u32 entry_size;
	struct hw_cq_s *cq;
	struct sli4_queue_s *queue;

	u32 use_count;
};

struct hw_wq_s {
	struct list_head list_entry;
	enum sli4_qtype_e type;		/*<< must be second */
	u32 instance;
	struct efct_hw_s *hw;

	u32 entry_count;
	u32 entry_size;
	struct hw_cq_s *cq;
	struct sli4_queue_s *queue;
	u32 class;
	u8 ulp;

	/* WQ consumed */
	u32 wqec_set_count;	/* how often IOs are
				 * submitted with wqce set
				 */
	u32 wqec_count;		/* current wqce counter */
	u32 free_count;		/* free count */
	u32 total_submit_count;	/* total submit count */
	struct list_head pending_list;	/* list of IOs pending for this WQ */

	/*
	 * Driver must quarantine XRIs
	 * for target writes and initiator read when using
	 * DIF separates. Throw them on a queue until another
	 * 4 similar requests are completed
	 * to ensure they are flushed from the internal chip cache
	 * before re-use. The must be a separate queue per CQ because
	 * the actual chip completion order cannot be determined.
	 * Since each WQ has a separate CQ, use the wq
	 * associated with the IO.
	 *
	 * Note: Protected by queue->lock
	 */
	struct efct_quarantine_info_s quarantine_info;

	/*
	 * HW IO allocated for use with Send Frame
	 */
	struct efct_hw_io_s *send_frame_io;

	/* Stats */
	u32 use_count;		/*<< use count */
	u32 wq_pending_count;	/* count of HW IOs that
				 * were queued on the WQ pending list
				 */
};

struct hw_rq_s {
	struct list_head list_entry;
	enum sli4_qtype_e type;			/*<< must be second */
	u32 instance;

	u32 entry_count;
	u32 use_count;
	u32 hdr_entry_size;
	u32 first_burst_entry_size;
	u32 data_entry_size;
	u8 ulp;
	bool is_mrq;
	u32 base_mrq_id;

	struct hw_cq_s *cq;

	u8 filter_mask;		/* Filter mask value */
	struct sli4_queue_s *hdr;
	struct sli4_queue_s *first_burst;
	struct sli4_queue_s *data;

	struct efc_hw_rq_buffer_s *hdr_buf;
	struct efc_hw_rq_buffer_s *fb_buf;
	struct efc_hw_rq_buffer_s *payload_buf;

	struct efc_hw_sequence_s **rq_tracker; /* RQ tracker for this RQ */
};

struct efct_hw_global_s {
	const char	*queue_topology_string;	/**< queue topo str */
};

extern struct efct_hw_global_s hw_global;

struct hw_eq_s *efct_hw_new_eq(struct efct_hw_s *hw, u32 entry_count);
struct hw_cq_s *efct_hw_new_cq(struct hw_eq_s *eq, u32 entry_count);
extern u32
efct_hw_new_cq_set(struct hw_eq_s *eqs[], struct hw_cq_s *cqs[],
		   u32 num_cqs, u32 entry_count);
struct hw_mq_s *efct_hw_new_mq(struct hw_cq_s *cq, u32 entry_count);
extern struct hw_wq_s
*efct_hw_new_wq(struct hw_cq_s *cq, u32 entry_count,
		u32 class, u32 ulp);
extern struct hw_rq_s
*efct_hw_new_rq(struct hw_cq_s *cq, u32 entry_count, u32 ulp);
extern u32
efct_hw_new_rq_set(struct hw_cq_s *cqs[], struct hw_rq_s *rqs[],
		   u32 num_rq_pairs,
		u32 entry_count, u32 ulp);
void efct_hw_del_eq(struct hw_eq_s *eq);
void efct_hw_del_cq(struct hw_cq_s *cq);
void efct_hw_del_mq(struct hw_mq_s *mq);
void efct_hw_del_wq(struct hw_wq_s *wq);
void efct_hw_del_rq(struct hw_rq_s *rq);
void efct_hw_queue_dump(struct efct_hw_s *hw);
void efct_hw_queue_teardown(struct efct_hw_s *hw);
int hw_route_rqe(struct efct_hw_s *hw, struct efc_hw_sequence_s *seq);
extern int
efct_hw_queue_hash_find(struct efct_queue_hash_s *hash, u16 id);
extern enum efct_hw_rtn_e
efct_hw_setup(struct efct_hw_s *hw, void *os, struct pci_dev *pdev);
enum efct_hw_rtn_e efct_hw_init(struct efct_hw_s *hw);
enum efct_hw_rtn_e efct_hw_teardown(struct efct_hw_s *hw);
enum efct_hw_rtn_e
efct_hw_reset(struct efct_hw_s *hw, enum efct_hw_reset_e reset);
int efct_hw_get_num_eq(struct efct_hw_s *hw);
extern enum efct_hw_rtn_e
efct_hw_get(struct efct_hw_s *hw, enum efct_hw_property_e prop, u32 *value);
extern void *
efct_hw_get_ptr(struct efct_hw_s *hw, enum efct_hw_property_e prop);
extern enum efct_hw_rtn_e
efct_hw_set(struct efct_hw_s *hw, enum efct_hw_property_e prop, u32 value);
extern enum efct_hw_rtn_e
efct_hw_set_ptr(struct efct_hw_s *hw, enum efct_hw_property_e prop,
		void *value);
extern int
efct_hw_event_check(struct efct_hw_s *hw, u32 vector);
extern int
efct_hw_process(struct efct_hw_s *hw, u32 vector, u32 max_isr_time_msec);
extern enum efct_hw_rtn_e
efct_hw_command(struct efct_hw_s *hw, u8 *cmd, u32 opts, void *cb,
		void *arg);
extern enum efct_hw_rtn_e
efct_hw_port_alloc(struct efc_lport *efc, struct efc_sli_port_s *sport,
		   struct efc_domain_s *domain, u8 *wwpn);
extern enum efct_hw_rtn_e
efct_hw_port_attach(struct efc_lport *efc, struct efc_sli_port_s *sport,
		    u32 fc_id);
extern enum efct_hw_rtn_e
efct_hw_port_control(struct efct_hw_s *hw, enum efct_hw_port_e ctrl,
		     uintptr_t value,
		void (*cb)(int status, uintptr_t value, void *arg),
		void *arg);
extern enum efct_hw_rtn_e
efct_hw_port_free(struct efc_lport *efc, struct efc_sli_port_s *sport);
extern enum efct_hw_rtn_e
efct_hw_domain_alloc(struct efc_lport *efc, struct efc_domain_s *domain,
		     u32 fcf, u32 vlan);
extern enum efct_hw_rtn_e
efct_hw_domain_attach(struct efc_lport *efc,
		      struct efc_domain_s *domain, u32 fc_id);
extern enum efct_hw_rtn_e
efct_hw_domain_free(struct efc_lport *efc, struct efc_domain_s *domain);
extern enum efct_hw_rtn_e
efct_hw_domain_force_free(struct efc_lport *efc, struct efc_domain_s *domain);
struct efc_domain_s *efct_hw_domain_get(struct efc_lport *efc, u16 fcfi);
extern enum efct_hw_rtn_e
efct_hw_node_alloc(struct efc_lport *efc, struct efc_remote_node_s *rnode,
		   u32 fc_addr, struct efc_sli_port_s *sport);
extern enum efct_hw_rtn_e
efct_hw_node_free_all(struct efct_hw_s *hw);
extern enum efct_hw_rtn_e
efct_hw_node_attach(struct efc_lport *efc, struct efc_remote_node_s *rnode,
		    struct efc_dma_s *sparms);
extern enum efct_hw_rtn_e
efct_hw_node_detach(struct efc_lport *efc, struct efc_remote_node_s *rnode);
extern enum efct_hw_rtn_e
efct_hw_node_free_resources(struct efc_lport *efc,
			    struct efc_remote_node_s *rnode);
struct efct_hw_io_s *efct_hw_io_alloc(struct efct_hw_s *hw);
extern struct efct_hw_io_s
*efct_hw_io_activate_port_owned(struct efct_hw_s *hw, struct efct_hw_io_s *io);
int efct_hw_io_free(struct efct_hw_s *hw, struct efct_hw_io_s *io);
u8 efct_hw_io_inuse(struct efct_hw_s *hw, struct efct_hw_io_s *io);
typedef int(*efct_hw_srrs_cb_t)(struct efct_hw_io_s *io,
				struct efc_remote_node_s *rnode, u32 length,
				int status, u32 ext_status, void *arg);

extern enum efct_hw_rtn_e
efct_hw_srrs_send(struct efct_hw_s *hw, enum efct_hw_io_type_e type,
		  struct efct_hw_io_s *io,
		  struct efc_dma_s *send, u32 len,
		  struct efc_dma_s *receive, struct efc_remote_node_s *rnode,
		  union efct_hw_io_param_u *iparam,
		  efct_hw_srrs_cb_t cb,
		  void *arg);
extern enum efct_hw_rtn_e
efct_hw_io_send(struct efct_hw_s *hw, enum efct_hw_io_type_e type,
		struct efct_hw_io_s *io, u32 len,
		union efct_hw_io_param_u *iparam,
		struct efc_remote_node_s *rnode, void *cb, void *arg);
extern enum efct_hw_rtn_e
efct_hw_io_register_sgl(struct efct_hw_s *hw, struct efct_hw_io_s *io,
			struct efc_dma_s *sgl,
			u32 sgl_count);
extern enum efct_hw_rtn_e
efct_hw_io_init_sges(struct efct_hw_s *hw,
		     struct efct_hw_io_s *io, enum efct_hw_io_type_e type);
extern enum efct_hw_rtn_e
efct_hw_io_add_seed_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
			struct efct_hw_dif_info_s *dif_info);
extern enum efct_hw_rtn_e
efct_hw_io_add_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
		   uintptr_t addr, u32 length);
extern enum efct_hw_rtn_e
efct_hw_io_add_dif_sge(struct efct_hw_s *hw, struct efct_hw_io_s *io,
		       uintptr_t addr);
extern enum efct_hw_rtn_e
efct_hw_io_abort(struct efct_hw_s *hw, struct efct_hw_io_s *io_to_abort,
		 bool send_abts, void *cb, void *arg);
extern int
efct_hw_io_get_xid(struct efct_hw_s *hw, struct efct_hw_io_s *io);
extern u32
efct_hw_io_get_count(struct efct_hw_s *hw,
		     enum efct_hw_io_count_type_e io_count_type);
extern u32
efct_hw_get_rqes_produced_count(struct efct_hw_s *hw);

extern enum efct_hw_rtn_e
efct_hw_firmware_write(struct efct_hw_s *hw, struct efc_dma_s *dma,
		       u32 size, u32 offset, int last,
		       void (*cb)(int status, u32 bytes_written,
				  u32 change_status, void *arg),
		       void *arg);

/* Function for retrieving SFP data */
extern enum efct_hw_rtn_e
efct_hw_get_sfp(struct efct_hw_s *hw, u16 page,
		void (*cb)(int, u32, u32 *, void *), void *arg);

/* Function for retrieving temperature data */
extern enum efct_hw_rtn_e
efct_hw_get_temperature(struct efct_hw_s *hw,
			void (*efct_hw_temp_cb_t)(int status,
						  u32 curr_temp,
				u32 crit_temp_thrshld,
				u32 warn_temp_thrshld,
				u32 norm_temp_thrshld,
				u32 fan_off_thrshld,
				u32 fan_on_thrshld,
				void *arg),
			void *arg);

/* Function for retrieving link statistics */
extern enum efct_hw_rtn_e
efct_hw_get_link_stats(struct efct_hw_s *hw,
		       u8 req_ext_counters,
		u8 clear_overflow_flags,
		u8 clear_all_counters,
		void (*efct_hw_link_stat_cb_t)(int status,
					       u32 num_counters,
			struct efct_hw_link_stat_counts_s *counters,
			void *arg),
		void *arg);
/* Function for retrieving host statistics */
extern enum efct_hw_rtn_e
efct_hw_get_host_stats(struct efct_hw_s *hw,
		       u8 cc,
		void (*efct_hw_host_stat_cb_t)(int status,
					       u32 num_counters,
			struct efct_hw_host_stat_counts_s *counters,
			void *arg),
		void *arg);

enum efct_hw_rtn_e efct_hw_raise_ue(struct efct_hw_s *hw, u8 dump);
extern enum efct_hw_rtn_e
efct_hw_dump_get(struct efct_hw_s *hw, struct efc_dma_s *dma, u32 size,
		 u32 offset,
		void (*cb)(int status, u32 bytes_read,
			   u8 eof, void *arg),
		void *arg);
extern enum efct_hw_rtn_e
efct_hw_set_dump_location(struct efct_hw_s *hw, u32 num_buffers,
			  struct efc_dma_s *dump_buffers, u8 fdb);

extern enum efct_hw_rtn_e
efct_hw_get_port_protocol(struct efct_hw_s *hw, u32 pci_func,
			  void (*mgmt_cb)(int status,
					  enum efct_hw_port_protocol_e
					  port_protocol,
			  void *arg),
		void *ul_arg);
extern enum efct_hw_rtn_e
efct_hw_set_port_protocol(struct efct_hw_s *hw,
			  enum efct_hw_port_protocol_e profile,
			  u32 pci_func,
			  void (*mgmt_cb)(int status,  void *arg),
			  void *ul_arg);

extern enum efct_hw_rtn_e
efct_hw_get_profile_list(struct efct_hw_s *hw,
			 void (*mgmt_cb)(int status,
					 struct efct_hw_profile_list_s *,
					 void *arg),
			 void *arg);
extern enum efct_hw_rtn_e
efct_hw_get_active_profile(struct efct_hw_s *hw,
			   void (*mgmt_cb)(int status,
					   u32 active_profile, void *arg),
			   void *arg);
extern enum efct_hw_rtn_e
efct_hw_set_active_profile(struct efct_hw_s *hw,
			   void (*mgmt_cb)(int status, void *arg),
			   u32 profile_id, void *arg);
extern enum efct_hw_rtn_e
efct_hw_get_nvparms(struct efct_hw_s *hw,
		    void (*mgmt_cb)(int status, u8 *wwpn,
				    u8 *wwnn, u8 hard_alpa,
				    u32 preferred_d_id, void *arg),
		    void *arg);
extern
enum efct_hw_rtn_e efct_hw_set_nvparms(struct efct_hw_s *hw,
				       void (*mgmt_cb)(int status, void *arg),
		u8 *wwpn, u8 *wwnn, u8 hard_alpa,
		u32 preferred_d_id, void *arg);
extern int
efct_hw_eq_process(struct efct_hw_s *hw, struct hw_eq_s *eq,
		   u32 max_isr_time_msec);
void efct_hw_cq_process(struct efct_hw_s *hw, struct hw_cq_s *cq);
extern void
efct_hw_wq_process(struct efct_hw_s *hw, struct hw_cq_s *cq,
		   u8 *cqe, int status, u16 rid);
extern void
efct_hw_xabt_process(struct efct_hw_s *hw, struct hw_cq_s *cq,
		     u8 *cqe, u16 rid);
int efct_hw_wq_write(struct hw_wq_s *wq, struct efct_hw_wqe_s *wqe);

extern enum efct_hw_rtn_e
efct_hw_dump_clear(struct efct_hw_s *hw,
		   void (*cb)(int status, void *arg),
		void *arg);

extern u8
efct_hw_is_io_port_owned(struct efct_hw_s *hw, struct efct_hw_io_s *io);

extern bool
efct_hw_is_xri_port_owned(struct efct_hw_s *hw, u32 xri);
extern struct efct_hw_io_s
*efct_hw_io_lookup(struct efct_hw_s *hw, u32 indicator);
extern u32
efct_hw_xri_move_to_port_owned(struct efct_hw_s *hw,
			       u32 num_xri);
extern enum
efct_hw_rtn_e efct_hw_xri_move_to_host_owned(struct efct_hw_s *hw,
					u8 num_xri);
extern int
efct_hw_reque_xri(struct efct_hw_s *hw, struct efct_hw_io_s *io);

struct efct_hw_send_frame_context_s {
	/* structure elements used by HW */
	struct efct_hw_s *hw;			/**> pointer to HW */
	struct hw_wq_callback_s *wqcb;	/**> WQ callback object, request tag */
	struct efct_hw_wqe_s wqe;	/* > WQE buf obj(may be queued
					 * on WQ pending list)
					 */
	void (*callback)(int status, void *arg);	/* > final
							 * callback function
							 */
	void *arg;			/**> final callback argument */

	/* General purpose elements */
	struct efc_hw_sequence_s *seq;
	struct efc_dma_s payload;	/**> a payload DMA buffer */
};

#define EFCT_HW_OBJECT_G5              0xfeaa0001
#define EFCT_HW_OBJECT_G6              0xfeaa0003
#define EFCT_FILE_TYPE_GROUP            0xf7
#define EFCT_FILE_ID_GROUP              0xa2
struct efct_hw_grp_hdr {
	u32 size;
	__be32	magic_number;
	u32 word2;
	u8 rev_name[128];
	u8 date[12];
	u8 revision[32];
};

enum efct_hw_rtn_e
efct_hw_send_frame(struct efct_hw_s *hw, struct fc_header_le_s *hdr,
		   u8 sof, u8 eof, struct efc_dma_s *payload,
		struct efct_hw_send_frame_context_s *ctx,
		void (*callback)(void *arg, u8 *cqe, int status),
		void *arg);

/* RQ completion handlers for RQ pair mode */
extern int
efct_hw_rqpair_process_rq(struct efct_hw_s *hw,
			  struct hw_cq_s *cq, u8 *cqe);
extern
enum efct_hw_rtn_e efct_hw_rqpair_sequence_free(struct efct_hw_s *hw,
						struct efc_hw_sequence_s *seq);
extern int
efct_hw_rqpair_process_auto_xfr_rdy_cmd(struct efct_hw_s *hw,
					struct hw_cq_s *cq, u8 *cqe);
extern int
efct_hw_rqpair_process_auto_xfr_rdy_data(struct efct_hw_s *hw,
					 struct hw_cq_s *cq, u8 *cqe);
extern enum efct_hw_rtn_e
efct_hw_rqpair_init(struct efct_hw_s *hw);
extern enum efct_hw_rtn_e
efct_hw_rqpair_auto_xfer_rdy_buffer_alloc(struct efct_hw_s *hw,
					  u32 num_buffers);
extern u8
efct_hw_rqpair_auto_xfer_rdy_buffer_post(struct efct_hw_s *hw,
					 struct efct_hw_io_s *io,
					bool reuse_buf);
extern enum efct_hw_rtn_e
efct_hw_rqpair_auto_xfer_rdy_move_to_port(struct efct_hw_s *hw,
					  struct efct_hw_io_s *io);
extern void
efct_hw_rqpair_auto_xfer_rdy_move_to_host(struct efct_hw_s *hw,
					  struct efct_hw_io_s *io);
void efct_hw_rqpair_teardown(struct efct_hw_s *hw);
void efct_hw_io_abort_all(struct efct_hw_s *hw);

enum efct_hw_rtn_e efct_hw_rx_allocate(struct efct_hw_s *hw);
enum efct_hw_rtn_e efct_hw_rx_post(struct efct_hw_s *hw);
void efct_hw_rx_free(struct efct_hw_s *hw);

typedef int
(*efct_hw_async_cb_t)(struct efct_hw_s *hw,
		      int status, u8 *mqe, void *arg);
extern int
efct_hw_async_call(struct efct_hw_s *hw,
		   efct_hw_async_cb_t callback, void *arg);

static inline void
efct_hw_sequence_copy(struct efc_hw_sequence_s *dst,
		      struct efc_hw_sequence_s *src)
{
	/* Copy src to dst, then zero out the linked list link */
	*dst = *src;
}

static inline enum efct_hw_rtn_e
efct_hw_sequence_free(struct efct_hw_s *hw, struct efc_hw_sequence_s *seq)
{
	/* Only RQ pair mode is supported */
	return efct_hw_rqpair_sequence_free(hw, seq);
}

/* HW WQ request tag API */
enum efct_hw_rtn_e efct_hw_reqtag_init(struct efct_hw_s *hw);
extern struct hw_wq_callback_s
*efct_hw_reqtag_alloc(struct efct_hw_s *hw,
			void (*callback)(void *arg, u8 *cqe,
					 int status), void *arg);
extern void
efct_hw_reqtag_free(struct efct_hw_s *hw, struct hw_wq_callback_s *wqcb);
extern struct hw_wq_callback_s
*efct_hw_reqtag_get_instance(struct efct_hw_s *hw, u32 instance_index);
void efct_hw_reqtag_reset(struct efct_hw_s *hw);

void efct_hw_io_free_internal(struct kref *arg);
void efct_hw_io_free_port_owned(struct kref *arg);
#define CPUTRACE(efct, str)

void efct_hw_workaround_setup(struct efct_hw_s *hw);

extern uint64_t
efct_get_wwn(struct efct_hw_s *hw, enum efct_hw_property_e prop);

#endif /* __EFCT_H__ */
