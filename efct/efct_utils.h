/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#ifndef __EFCT_UTILS_H__
#define __EFCT_UTILS_H__

/* Sparse vector structure. */
struct sparse_vector_s {
	void *os;
	u32 max_idx;		/**< maximum index value */
	void **array;			/**< pointer to 3D array */
};

#define EFCT_RAMLOG_DEFAULT_BUFFERS		5
/* Allocate maximum allowed (4M) */
#define EFCT_Q_HIST_SIZE (1000000UL)		/* Size in words */

#define EFCT_LOG_ENABLE_SM_TRACE(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 0)) != 0) : 0)
#define EFCT_LOG_ENABLE_ELS_TRACE(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 1)) != 0) : 0)
#define EFCT_LOG_ENABLE_SCSI_TRACE(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 2)) != 0) : 0)
#define EFCT_LOG_ENABLE_SCSI_TGT_TRACE(efct)	\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 3)) != 0) : 0)
#define EFCT_LOG_ENABLE_DOMAIN_SM_TRACE(efct)	\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 4)) != 0) : 0)
#define EFCT_LOG_ENABLE_Q_FULL_BUSY_MSG(efct)	\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 5)) != 0) : 0)
#define EFCT_LOG_ENABLE_IO_ERRORS(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 6)) != 0) : 0)
#define EFCT_LOG_ENABLE_LIO_IO_TRACE(efct)	\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 7)) != 0) : 0)
#define EFCT_LOG_ENABLE_LIO_TRACE(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 8)) != 0) : 0)
#define EFCT_LOG_ENABLE_CPU_TRACE(efct)		\
		(((efct) != NULL) ? (((efct)->logmask & (1U << 9)) != 0) : 0)
#define EFCT_DEBUG_ALWAYS		BIT(0)
#define EFCT_DEBUG_ENABLE_MQ_DUMP	BIT(1)
#define EFCT_DEBUG_ENABLE_CQ_DUMP	BIT(2)
#define EFCT_DEBUG_ENABLE_WQ_DUMP	BIT(3)
#define EFCT_DEBUG_ENABLE_EQ_DUMP	BIT(4)
#define EFCT_DEBUG_ENABLE_SPARAM_DUMP	BIT(5)

#define SPV_ROWLEN	256
#define SPV_DIM		3

struct efct_pool_s {
	void *os;
	struct efct_array_s *a;
	struct list_head freelist;
	bool use_lock;
	/* Protects freelist */
	spinlock_t lock;
};

extern void
efct_array_set_slablen(u32 len);
extern struct efct_array_s *
efct_array_alloc(void *os, u32 size, u32 count);
extern void
efct_array_free(struct efct_array_s *array);
extern void *
efct_array_get(struct efct_array_s *array, u32 idx);
extern u32
efct_array_get_count(struct efct_array_s *array);
extern u32
efct_array_get_size(struct efct_array_s *array);

extern struct efct_varray_s *
efct_varray_alloc(void *os, u32 entry_count);
extern void
efct_varray_free(struct efct_varray_s *ai);
extern int
efct_varray_add(struct efct_varray_s *ai, void *entry);
extern void
efct_varray_iter_reset(struct efct_varray_s *ai);
extern void *
efct_varray_iter_next(struct efct_varray_s *ai);
extern void *
_efct_varray_iter_next(struct efct_varray_s *ai);
extern void
efct_varray_unlock(struct efct_varray_s *ai);
extern u32
efct_varray_get_count(struct efct_varray_s *ai);

/**
 * @brief Sparse Vector API
 *
 * This is a trimmed down sparse vector implementation tuned to the problem of
 * 24-bit FC_IDs. In this case, the 24-bit index value is broken down in three
 * 8-bit values. These values are used to index up to three 256 element arrays.
 * Arrays are allocated, only when needed. @n @n
 * The lookup can complete in constant time (3 indexed array references). @n @n
 * A typical use case would be that the fabric/directory FC_IDs would cause two
 * rows to be allocated, and the fabric assigned remote nodes would cause two
 * rows to be allocated, with the root row always allocated. This gives five
 * rows of 256 x sizeof(void*), resulting in 10k.
 */
/*!
 * @defgroup spv Sparse Vector
 */

void efct_spv_del(struct sparse_vector_s *spv);
struct sparse_vector_s *efct_spv_new(void *os);
void efct_spv_set(struct sparse_vector_s *sv, u32 idx, void *value);
void *efct_spv_get(struct sparse_vector_s *sv, u32 idx);

/**
 * @POOL
 *
 */
extern struct efct_pool_s *
efct_pool_alloc(void *os, u32 size, u32 count,
		bool use_lock);
extern void
efct_pool_reset(struct efct_pool_s *pool);
extern void
efct_pool_free(struct efct_pool_s *pool);
extern void *
efct_pool_get(struct efct_pool_s *pool);
extern void
efct_pool_put(struct efct_pool_s *pool, void *arg);
extern void
efct_pool_put_head(struct efct_pool_s *pool, void *arg);
extern u32
efct_pool_get_count(struct efct_pool_s *pool);
extern void *
efct_pool_get_instance(struct efct_pool_s *pool, u32 instance);
extern u32
efct_pool_get_freelist_count(struct efct_pool_s *pool);
#endif /* __EFCT_UTILS_H__ */
