/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#ifndef __EFCT_HW_QUEUES_H__
#define __EFCT_HW_QUEUES_H__

#define EFCT_HW_MQ_DEPTH	128
#include "efct_hw.h"

enum efct_hw_qtop_entry_e {
	QTOP_EQ = 0,
	QTOP_CQ,
	QTOP_WQ,
	QTOP_RQ,
	QTOP_MQ,
	QTOP_LAST,
};

struct efct_hw_qtop_entry_s {
	enum efct_hw_qtop_entry_e entry;
	bool set_default;
	u32 len;
	u8 class;
	u8 ulp;
	u8 filter_mask;
};

struct efct_hw_mrq_s {
	struct rq_config {
		struct hw_eq_s *eq;
		u32 len;
		u8 class;
		u8 ulp;
		u8 filter_mask;
	} rq_cfg[16];
	u32 num_pairs;
};

#define MAX_TOKENS			256
#define EFCT_HW_MAX_QTOP_ENTRIES	200

struct efct_hw_qtop_s {
	void *os;
	struct efct_hw_qtop_entry_s *entries;
	u32 alloc_count;
	u32 inuse_count;
	u32 entry_counts[QTOP_LAST];
	u32 rptcount[10];
	u32 rptcount_idx;
};

struct efct_hw_qtop_s *
efct_hw_qtop_parse(struct efct_hw_s *hw, const char *qtop_string);
void efct_hw_qtop_free(struct efct_hw_qtop_s *qtop);
const char *efct_hw_qtop_entry_name(enum efct_hw_qtop_entry_e entry);
u32 efct_hw_qtop_eq_count(struct efct_hw_s *hw);

enum efct_hw_rtn_e
efct_hw_init_queues(struct efct_hw_s *hw, struct efct_hw_qtop_s *qtop);
void hw_thread_eq_handler(struct efct_hw_s *hw, struct hw_eq_s *eq,
			  u32 max_isr_time_msec);
extern  struct hw_wq_s
*efct_hw_queue_next_wq(struct efct_hw_s *hw, struct efct_hw_io_s *io);

#endif /* __EFCT_HW_QUEUES_H__ */
