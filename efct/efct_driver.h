/*******************************************************************
 * This file is part of the Emulex Linux Device Driver for         *
 * Fibre Channel Host Bus Adapters.                                *
 * Copyright (C) 2018 Broadcom. All Rights Reserved. The term	   *
 * “Broadcom” refers to Broadcom Inc. and/or its subsidiaries.     *
 *                                                                 *
 * This program is free software; you can redistribute it and/or   *
 * modify it under the terms of version 2 of the GNU General       *
 * Public License as published by the Free Software Foundation.    *
 * This program is distributed in the hope that it will be useful. *
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND          *
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,  *
 * FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE      *
 * DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD *
 * TO BE LEGALLY INVALID.  See the GNU General Public License for  *
 * more details, a copy of which can be found in the file COPYING  *
 * included with this package.                                     *
 ********************************************************************/

#if !defined(__EFCT_DRIVER_H__)
#define __EFCT_DRIVER_H__

/***************************************************************************
 * OS specific includes
 */
#include <stdarg.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <asm-generic/ioctl.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/dma-mapping.h>
#include <linux/bitmap.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <asm/byteorder.h>
#include <linux/timer.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/cacheflush.h>
#include <linux/pagemap.h>
#include <linux/kthread.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/random.h>
#include <linux/sched.h>
#include <linux/jiffies.h>
#include <linux/ctype.h>
#include <linux/debugfs.h>
#include <linux/firmware.h>
#include <linux/sched/signal.h>

#define MODPARAM_DEF_INITIATOR		0
#define MODPARAM_DEF_TARGET		1
#define MODPARAM_DEF_HOLDOFF_LINK_ONLINE 2

#define EFCT_DRIVER_NAME	"efct"
#define EFCT_DRIVER_VERSION	"849.999.999.999"

/* EFCT_OS_MAX_ISR_TIME_MSEC -
 * maximum time driver code should spend in an interrupt
 * or kernel thread context without yielding
 */
#define EFCT_OS_MAX_ISR_TIME_MSEC			1000

/**
 * Common (transport agnostic) shared declarations
 */

#define EFCT_CTRLMASK_XPORT_DISABLE_AUTORSP_TSEND       BIT(0)
#define EFCT_CTRLMASK_XPORT_DISABLE_AUTORSP_TRECEIVE    BIT(1)
#define EFCT_CTRLMASK_XPORT_ENABLE_TARGET_RSCN          BIT(3)
#define EFCT_CTRLMASK_TGT_ALWAYS_VERIFY_DIF             BIT(4)
#define EFCT_CTRLMASK_TGT_SET_DIF_REF_TAG_CRC           BIT(5)
#define EFCT_CTRLMASK_TEST_CHAINED_SGLS                 BIT(6)
#define EFCT_CTRLMASK_ISCSI_ISNS_ENABLE                 BIT(7)
#define EFCT_CTRLMASK_ENABLE_FABRIC_EMULATION           BIT(8)
#define EFCT_CTRLMASK_INHIBIT_INITIATOR                 BIT(9)
#define EFCT_CTRLMASK_CRASH_RESET                       BIT(10)

/*
 * LOG_CRIT:	The hardware is unusable (chip in UE state, failures to
 *		write to PCI registers)
 * LOG_ERR:	An error occurred that may prevent normal operation
 *		(out of memory, startup failures)
 * LOG_WARN:	An error has occurred.  The driver is functional but the
 *		requested operation
 *		may not have worked as expected (limits exceeded, I/O errors)
 * LOG_INFO:	Normal operation of something interesting
 *		(driver start, link up/down)
 * LOG_TEST:	An error has occurred and has been handled.
 *		This is normal operation in a production
 *		environment but may be of interest to test.
 * LOG_DEBUG:	Information of interest to developers.
 * Normal operation has occurred.
 */
#define LOG_CRIT	0
#define LOG_ERR		1
#define LOG_WARN	2
#define LOG_INFO	3
#define LOG_TEST	4
#define LOG_DEBUG	5

const char *efct_display_name(void *os);
void _efct_log(void *os, const char *func,
	       int line, const char *fmt, ...);
extern int loglevel;

#define efct_log_crit(os, fmt, ...) efct_log(os, LOG_CRIT, fmt, ##__VA_ARGS__)
#define efct_log_err(os, fmt, ...) efct_log(os, LOG_ERR, fmt, ##__VA_ARGS__)
#define efct_log_warn(os, fmt, ...) efct_log(os, LOG_WARN, fmt, ##__VA_ARGS__)
#define efct_log_info(os, fmt, ...) efct_log(os, LOG_INFO, fmt, ##__VA_ARGS__)
#define efct_log_test(os, fmt, ...) efct_log(os, LOG_TEST, fmt, ##__VA_ARGS__)
#define efct_log_debug(os, fmt, ...) efct_log(os, LOG_DEBUG, fmt, ##__VA_ARGS__)

#define efct_log(os, level, fmt, ...)	\
	do {						\
		if (level <= loglevel) {		\
			pr_err("%s:%s:%i:%s:" fmt, EFCT_DRIVER_NAME, __func__, \
			__LINE__, (os) ? efct_display_name(os) : \
			"", ##__VA_ARGS__);\
		}					\
	} while (0)

#define efct_assert(cond, ...) \
	do {			\
		if (!(cond)) { \
			WARN_ON(!(cond)); \
		}		\
	} while (0)

#define EFCT_FC_RQ_SIZE_DEFAULT			1024
#define EFCT_FC_MAX_SGL			64
#define EFCT_FC_DIF_SEED			0

/* Timeouts */
#define EFCT_FC_ELS_SEND_DEFAULT_TIMEOUT	0
#define EFCT_FC_ELS_DEFAULT_RETRIES		3
#define EFCT_FC_FLOGI_TIMEOUT_SEC		5
#define EFCT_FC_DOMAIN_SHUTDOWN_TIMEOUT_USEC    30000000 /* 30 seconds */

/* Watermark */
#define EFCT_WATERMARK_HIGH_PCT			90
#define EFCT_WATERMARK_LOW_PCT			80
#define EFCT_IO_WATERMARK_PER_INITIATOR		8

#include "efct_utils.h"
#include "../libefc/efc_fcp.h"
#include "../libefc/efclib.h"
#include "efct_hw.h"
#include "efct_io.h"
#include "efct_xport.h"

struct efct_s {
	struct efct_os_s efct_os;
	struct pci_dev	*pdev;
	char display_name[EFC_DISPLAY_NAME_LENGTH];
	struct efct_io_pool_s *io_pool;	/**< pointer to IO pool */
	bool attached;
	struct efct_scsi_tgt_s tgt_efct;
	struct efct_xport_s *xport;	/*>> Pointer to transport object */
	struct efc_lport *efcport;	/* Discovery library object */
	struct Scsi_Host *shost;	/*Scsi host for fc_host entries*/
	int ctrlmask;
	int logmask;
	u32 max_isr_time_msec;	/*>> Maximum ISR time */
	/**< for error injection testing */
	enum efc_err_injection_e err_injection;
	/**< specific cmd to inject error into */
	u32 cmd_err_inject;
	time_t delay_value_msec;		/**< for injecting delays */

	const char *desc;
	u32 instance_index;
	u16 pci_vendor;
	u16 pci_device;
	u16 pci_subsystem_vendor;
	u16 pci_subsystem_device;
	char businfo[EFC_DISPLAY_BUS_INFO_LENGTH];

	const char *model;
	const char *driver_version;
	const char *fw_version;

	struct efct_hw_s hw;

	bool external_loopback;
	u32 num_vports;
	u32 rq_selection_policy;
	char *filter_def;

	bool soft_wwn_enable;

	/*
	 * tgt_rscn_delay - delay in kicking off RSCN processing
	 * (nameserver queries) after receiving an RSCN on the
	 * target. This prevents thrashing of nameserver
	 * requests due to a huge burst of RSCNs received in a
	 * short period of time
	 * Note: this is only valid when target RSCN handling
	 * is enabled -- see ctrlmask.
	 */
	time_t tgt_rscn_delay_msec;	/*>> minimum target RSCN delay */

	/*
	 * tgt_rscn_period - determines maximum frequency when
	 * processing back-to-back
	 * RSCNs; e.g. if this value is 30, there will never be any
	 * more than 1 RSCN handling per 30s window. This prevents
	 * initiators on a faulty link generating
	 * many RSCN from causing the target to continually query the
	 * nameserver.
	 * Note:this is only valid when target RSCN handling is enabled
	 */
	time_t tgt_rscn_period_msec;	/*>> minimum target RSCN period */

	/*
	 * Target IO timer value:
	 * Zero: target command timeout disabled.
	 * Non-zero: Timeout value, in seconds, for target commands
	 */
	u32 target_io_timer_sec;

	int speed;
	int topology;
	u32 nodedb_mask;		/*>> Node debugging mask */

	bool enable_numa_support;	/* NUMA support enabled */
	u8 efct_req_fw_upgrade;
	u16 sw_feature_cap;
	struct dentry *sess_debugfs_dir;
};

#define MAX_EFCT_DEVICES  64
extern struct efct_s *efct_devices[MAX_EFCT_DEVICES];

#define efct_is_fc_initiator_enabled()	(efct->enable_ini)
#define efct_is_fc_target_enabled()	(efct->enable_tgt)

struct efct_s *efct_get_instance(u32 index);
void efct_stop_event_processing(struct efct_os_s *efct_os);
int efct_start_event_processing(struct efct_os_s *efct_os);

void *efct_device_alloc(u32 nid);
int efct_device_interrupts_required(struct efct_s *efct);
int efct_device_attach(struct efct_s *efct);
int efct_device_detach(struct efct_s *efct);
void efct_device_free(struct efct_s *efct);
int efct_device_ioctl(struct efct_s *efct, unsigned int cmd,
		      unsigned long arg);
int efct_device_init(void);
void efct_device_init_complete(void);
void efct_device_shutdown(void);
void efct_device_shutdown_complete(void);
int efct_request_firmware_update(struct efct_s *efct);

#endif /* __EFCT_DRIVER_H__ */
